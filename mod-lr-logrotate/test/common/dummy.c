/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <time.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>

#include "mock.h"
#include "logrotate_utils.h"

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxc_var_dump(args, 2);
    return 0;
}

int __wrap_amxp_dir_scan(const char* path,
                         const char* filter,
                         bool recursive,
                         amxp_dir_match_fn_t fn,
                         void* priv) {
    assert_string_equal(path, "/var/log");
    assert_string_equal(filter, "d_type == DT_REG && d_name matches 'messages(\\..*)?$'");
    assert_false(recursive);
    fn("/var/log/messages.1", priv);
    fn("/var/log/messages.2", priv);
    return 0;
}

int __wrap_stat(const char* path, struct stat* buf) {
    assert_non_null(path);

    buf->st_size = 542;
    time(&buf->st_mtime);

    return 0;
}

int __wrap_system(const char* command) {
    ++total_system_calls;
    assert_non_null(command);
    return 0;
}

int __wrap_remove(const char* pathname) {
    ++total_removes;
    assert_non_null(pathname);
    return 0;
}

int __wrap_access(const char* pathname, int mode) {
    assert_int_equal(mode, F_OK);

    if(strcmp(pathname, "/etc/crontabs/root") == 0) {
        return 0;
    }

    return -1;
}

char* __wrap_realpath(const char* restrict path, UNUSED char* restrict resolved_path) {
    assert_non_null(path);
    char* retval = strdup(path);
    return retval;
}

int write_to_file(const char* filename, const char* data) {
    ++total_write_to_file;

    if(strcmp(filename, "/etc/crontabs/logrotate") == 0) {
        assert_non_null(strstr(data, "#############################  START of logrotate crontabs #############################\n"));
        assert_non_null(strstr(data, "# tr181-deviceinfo logrotate generated crontabs.\n"));
        assert_non_null(strstr(data, "#    do not change any values in this logrotate crontab block.\n"));
        assert_non_null(strstr(data, "/usr/sbin/logrotate /etc/logrotate.conf\n"));
    } else { // Logrotate config file
        if(compress) {
            assert_non_null(strstr(data, "\tcompress"));
        } else {
            assert_null(strstr(data, "\tcompress"));
        }

        if(delay_compress) {
            assert_non_null(strstr(data, "\tdelaycompress"));
        } else {
            assert_null(strstr(data, "\tdelaycompress"));
        }

        if(copy_truncate) {
            assert_non_null(strstr(data, "\tcopytruncate"));
            assert_null(strstr(data, "\tcreate"));
        } else {
            assert_null(strstr(data, "\tcopytruncate"));
            assert_non_null(strstr(data, "\tcreate"));
        }

        if(su_used) {
            assert_non_null(strstr(data, "\tsu root root"));
        }

        assert_non_null(strstr(data, "{\n"));
        assert_non_null(strstr(data, "\tnotifempty\n"));
        assert_non_null(strstr(data, "\tmaxsize "));
        assert_non_null(strstr(data, "\tmissingok\n"));
        assert_non_null(strstr(data, "\tlastaction\n"));
        assert_non_null(strstr(data, "\t\t/usr/lib/debuginfo/logrotate_add.sh $1 "));
        assert_non_null(strstr(data, " > /dev/null\n"));
        assert_non_null(strstr(data, "\tendscript\n"));
        assert_non_null(strstr(data, "\trotate "));
        assert_non_null(strstr(data, "}"));
    }
    return 0;
}