/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_logrotate.h"
#include "logrotate_lr.h"
#include "logrotate_utils.h"

#define COMPRESSION_CMD_PATH "/usr/bin/"
#define LOGROTATE_CONF_DIR "/etc/logrotate.d/"
#define SYSLOG_NG_CTL_PATH "/usr/sbin/syslog-ng-ctl"

#define SYSLOG_IDENTIFIER "syslog-"

static int generate_compression_config(logrotate_entry_t* entry, amxc_string_t* config) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null(entry, exit);
    when_null(config, exit);
    when_true_status(entry->compression == NONE, exit, rv = 0);

    amxc_string_appendf(config, "\tcompress\n");
    if(entry->delay_compression) {
        amxc_string_appendf(config, "\tdelaycompress\n");
    }

    switch(entry->compression) {
    case GZIP:
        break;
    case BZIP2:
        amxc_string_appendf(config, "\tcompresscmd " COMPRESSION_CMD_PATH "bzip2\n");
        amxc_string_appendf(config, "\tuncompresscmd " COMPRESSION_CMD_PATH "bunzip2\n");
        amxc_string_appendf(config, "\tcompressext .bz2\n");
        break;
    case LZO:
        amxc_string_appendf(config, "\tcompresscmd "  "/bin/lzop\n");
        amxc_string_appendf(config, "\tuncompresscmd " COMPRESSION_CMD_PATH "unlzop\n");
        amxc_string_appendf(config, "\tcompressext .lzo\n");
        break;
    case XZ:
        amxc_string_appendf(config, "\tcompresscmd " COMPRESSION_CMD_PATH "xz\n");
        amxc_string_appendf(config, "\tuncompresscmd " COMPRESSION_CMD_PATH "unxz\n");
        amxc_string_appendf(config, "\tcompressext .xz\n");
        break;
    case ZSTD:
        amxc_string_appendf(config, "\tcompresscmd " COMPRESSION_CMD_PATH "zstd\n");
        amxc_string_appendf(config, "\tuncompresscmd " COMPRESSION_CMD_PATH "unzstd\n");
        amxc_string_appendf(config, "\tcompressext .zst\n");
        break;
    default:
        when_false_trace(false, exit, ERROR, "Invalid compression option found: %d", entry->compression);
        break;
    }

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int generate_scripts_config(logrotate_entry_t* entry, amxc_string_t* config) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    const char* buffer = NULL;
    bool is_syslog_entry = (strncmp(entry->alias, SYSLOG_IDENTIFIER, strlen(SYSLOG_IDENTIFIER)) == 0);

    when_null(entry, exit);
    when_null(config, exit);

    buffer = get_script(LR_FIRSTACTION_SCRIPT);
    if(!STR_EMPTY(buffer) && file_exists(get_script(LR_FIRSTACTION_SCRIPT))) {
        amxc_string_appendf(config, "\t" LR_FIRSTACTION_SCRIPT "\n");
        amxc_string_appendf(config, "\t\t%s $1 %s > /dev/null\n", buffer, entry->alias); // $1 contains filename of rotated log file
        amxc_string_appendf(config, "\tendscript\n");
    }

    buffer = get_script(LR_PREROTATE_SCRIPT);
    if(!STR_EMPTY(buffer) && file_exists(get_script(LR_PREROTATE_SCRIPT))) {
        amxc_string_appendf(config, "\t" LR_PREROTATE_SCRIPT "\n");
        amxc_string_appendf(config, "\t\t%s $1 %s > /dev/null\n", buffer, entry->alias); // $1 contains filename of rotated log file
        amxc_string_appendf(config, "\tendscript\n");
    }

    buffer = get_script(LR_POSTROTATE_SCRIPT);
    if(is_syslog_entry || (!STR_EMPTY(buffer) && file_exists(get_script(LR_POSTROTATE_SCRIPT)))) {
        amxc_string_appendf(config, "\t" LR_POSTROTATE_SCRIPT "\n");
        if(!STR_EMPTY(buffer) && file_exists(get_script(LR_POSTROTATE_SCRIPT))) {
            amxc_string_appendf(config, "\t\t%s $1 %s > /dev/null\n", buffer, entry->alias); // $1 contains filename of rotated log file
        }
        if(is_syslog_entry) {
            amxc_string_appendf(config, "\t\t" SYSLOG_NG_CTL_PATH " reload > /dev/null\n");
        }
        amxc_string_appendf(config, "\tendscript\n");
    }

    buffer = get_script(LR_LASTACTION_SCRIPT);
    amxc_string_appendf(config, "\t" LR_LASTACTION_SCRIPT "\n");
    if(!STR_EMPTY(buffer) && file_exists(get_script(LR_LASTACTION_SCRIPT))) {
        amxc_string_appendf(config, "\t\t%s $1 %s > /dev/null\n", buffer, entry->alias);                // $1 contains filename of rotated log file
    }
    if(allow_add_script()) {
        amxc_string_appendf(config, "\t\t%s $1 %s > /dev/null\n", get_script(LR_ADD_SCRIPT), entry->alias); // $1 contains filename of rotated log file
    }
    amxc_string_appendf(config, "\tendscript\n");

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static char* generate_logrotate_configfile(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t config;
    char* buffer = NULL;
    char size_order = 'k';
    uint32_t size = 0;

    amxc_string_init(&config, 0);

    when_null(entry, exit);

    size = entry->max_file_size;
    size_order = convert_filesize(&size);

    amxc_string_setf(&config, "%s {\n", entry->name);
    when_failed(generate_compression_config(entry, &config), exit);
    amxc_string_appendf(&config, "\tnotifempty\n");                     // Do not rotate the log if it is empty
    amxc_string_appendf(&config, "\tmaxsize %d%c\n", size, size_order); // Log files are rotated only if they grow bigger then size bytes
                                                                        // even before the additionally specified time interval (daily, weekly, monthly, or yearly).
                                                                        // If size is followed by k, the size is assumed to be in kilobytes.
    amxc_string_appendf(&config, "\tmissingok\n");                      // If the log file is missing, go on to the next one without issuing an error message

    if(entry->copy_truncate) {
        amxc_string_appendf(&config, "\tcopytruncate\n");               //Truncate the original log file in place after creating a copy, instead of moving the old log file and optionally creating a new one
    } else {
        amxc_string_appendf(&config, "\tcreate\n");                     // Immediately after rotation (before the postrotate script is run) the log file is created (with the same name as the log file just rotated)
    }
    if((entry->user != NULL) && (entry->group != NULL)) {
        amxc_string_appendf(&config, "\tsu %s %s\n", entry->user, entry->group);
    }
    when_failed(generate_scripts_config(entry, &config), exit);
    amxc_string_appendf(&config, "\trotate %d\n", entry->number_of_files); // Log files are rotated count times before being removed
    amxc_string_appendf(&config, "}");
    buffer = amxc_string_take_buffer(&config);

exit:
    amxc_string_clean(&config);
    return buffer;
    SAH_TRACEZ_OUT(ME);
}

int create_logrotate_config(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    char* config = NULL;
    amxc_string_t config_path;

    amxc_string_init(&config_path, 0);

    when_false(entry, exit);

    amxc_string_setf(&config_path, LOGROTATE_CONF_DIR "%s.conf", entry->alias);
    entry->config_file_path = amxc_string_take_buffer(&config_path);

    config = generate_logrotate_configfile(entry);
    when_str_empty_trace(config, exit, ERROR, "Could not generate logrotate config file");
    rv = write_to_file(entry->config_file_path, config);

exit:
    amxc_string_clean(&config_path);
    free(config);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int delete_logrotate_config(logrotate_entry_t* entry) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    when_null(entry, exit);
    when_str_empty(entry->config_file_path, exit);

    rv = remove(entry->config_file_path);
    free(entry->config_file_path);
    entry->config_file_path = NULL;

exit:
    return rv;
    SAH_TRACEZ_OUT(ME);
}