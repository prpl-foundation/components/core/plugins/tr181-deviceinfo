/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo_vendor_log_file.h"
#include "deviceinfo_logrotate.h"
#include "filetransfer/filetransfer.h"
#include "deviceinfo.h"

#define VENDORLOGFILE_PATH "DeviceInfo.VendorLogFile."

/*
 * Called at the end of dm_update_vendorlogfile.
 * This function is responsible for adding the VendorLogFileRef of the newly created DeviceInfo.VendorLogFile instances to the correct Syslog dataset.
 */
static void match_vendor_logfile_refs(amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* root_obj = deviceinfo_get_root();
    amxd_object_t* deviceinfo_vendor_logfile_template_obj = amxd_object_findf(root_obj, "VendorLogFile.");
    when_null_trace(deviceinfo_vendor_logfile_template_obj, exit, ERROR, "Could not get VendorLogFile template object");

    // Loop over all DeviceInfo.VendorLogFile instances
    amxd_object_for_each(instance, obj_it, deviceinfo_vendor_logfile_template_obj) {
        amxd_object_t* deviceinfo_vendor_logfile_instance_obj = amxc_container_of(obj_it, amxd_object_t, it);
        char* deviceinfo_vendor_logfile_inst_name = amxd_object_get_value(cstring_t, deviceinfo_vendor_logfile_instance_obj, "Name", NULL);

        // Loop over list of all parameters used to update these VendorLogFile instances
        amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
            amxc_var_t* parameters = amxc_var_from_llist_it(it);
            const char* syslog_logfile_file_path = GET_CHAR(parameters, "SyslogActionLogFilePath");
            const char* action = GET_CHAR(parameters, "Action");

            // When this VendorLogFile instance was added and the SyslogActionLogFilePath matches the VendorLogFile name, update the SyslogVendorLogFileRef
            if((strcmp(action, "add") == 0) && (strcmp(deviceinfo_vendor_logfile_inst_name, syslog_logfile_file_path) == 0)) {
                amxc_string_t buffer;
                amxc_var_t* syslog_logfile_vendor_logfile_ref_key = amxc_var_get_key(parameters, "SyslogVendorLogFileRef", AMXC_VAR_FLAG_DEFAULT);

                amxc_string_init(&buffer, 0);

                if(syslog_logfile_vendor_logfile_ref_key == NULL) {
                    amxc_string_clean(&buffer);
                    free(deviceinfo_vendor_logfile_inst_name);
                    goto exit;
                }
                amxc_string_setf(&buffer, VENDORLOGFILE_PATH "%s.", amxd_object_get_name(deviceinfo_vendor_logfile_instance_obj, AMXD_OBJECT_INDEXED));

                amxc_var_set(cstring_t, syslog_logfile_vendor_logfile_ref_key, amxc_string_get(&buffer, 0));
                amxc_string_clean(&buffer);
                break;
            }
        }
        free(deviceinfo_vendor_logfile_inst_name);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int dm_update_vendorlogfile(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* root_obj = deviceinfo_get_root();
    int retval = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null(args, exit);
    when_null_trace(root_obj, exit, ERROR, "Could not get DeviceInfo object");

    amxc_llist_iterate(it, amxc_var_constcast(amxc_llist_t, args)) {
        amxc_var_t* parameters = amxc_var_from_llist_it(it);
        const char* action = GET_CHAR(parameters, "Action");
        const char* file_path = GET_CHAR(parameters, "SyslogActionLogFilePath");

        if(strcmp(action, "update") == 0) {
            when_str_empty_trace(file_path, exit, ERROR, "File path not specified");
            when_failed_trace(amxd_trans_select_pathf(&trans, "%s", GET_CHAR(parameters, "SyslogVendorLogFileRef")), exit, ERROR, "Could not select VendorLogFile for update");
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_set_param(&trans, "Name", GET_ARG(parameters, "SyslogActionLogFilePath"));
        } else if(strcmp(action, "add") == 0) {
            when_str_empty_trace(file_path, exit, ERROR, "File path not specified");
            if(amxd_object_findf(root_obj, "VendorLogFile.[Name == \"%s\"].", file_path) == NULL) {
                // New file, add instance
                amxd_trans_select_pathf(&trans, VENDORLOGFILE_PATH);
                amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
                amxd_trans_add_inst(&trans, 0, NULL);
                amxd_trans_set_param(&trans, "Alias", GET_ARG(parameters, "SyslogActionAlias"));
            } else {
                // Existing instance update it
                amxd_trans_select_pathf(&trans, "DeviceInfo.VendorLogFile.[Name == \"%s\"].", file_path);
            }
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_set_param(&trans, "Name", GET_ARG(parameters, "SyslogActionLogFilePath"));
            amxd_trans_set_param(&trans, "MaximumSize", GET_ARG(parameters, "MaximumSize"));
            amxd_trans_set_param(&trans, "Persistent", GET_ARG(parameters, "Persistent"));
        } else if(strcmp(action, "remove") == 0) {
            amxd_trans_select_pathf(&trans, VENDORLOGFILE_PATH);
            amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
            amxd_trans_del_inst(&trans, 0, amxd_object_get_name(amxd_object_findf(root_obj, "VendorLogFile.[Name == \"%s\"].", file_path), AMXD_OBJECT_NAMED));
        }
    }

    retval = amxd_trans_apply(&trans, deviceinfo_get_dm());
    when_failed_trace(retval, exit, ERROR, "Failed to update VendorLogfile entries retval=%d", retval);

    match_vendor_logfile_refs(args);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool upload_cb(ftx_request_t* req, void* userdata) {
    char* transfer_url = (char*) userdata;
    amxb_bus_ctx_t* ctx = deviceinfo_get_busctx();
    amxc_ts_t* ts_start = NULL;
    amxc_ts_t* ts_end = NULL;
    char time_start_str[64] = {0};
    char time_end_str[64] = {0};
    ftx_error_code_t error_code;
    amxc_var_t* ret = NULL;
    amxc_var_t* event_params = NULL;

    error_code = ftx_request_get_error_code(req);
    ts_start = ftx_request_get_start_time(req);
    ts_end = ftx_request_get_end_time(req);
    amxc_ts_format(ts_start, time_start_str, sizeof(time_start_str));
    amxc_ts_format(ts_end, time_end_str, sizeof(time_end_str));

    amxc_var_new(&event_params);
    amxc_var_set_type(event_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, event_params, "TransferType", "Upload");
    amxc_var_add_key(uint32_t, event_params, "FaultCode", error_code);
    amxc_var_add_key(cstring_t, event_params, "FaultString", ftx_request_get_error_reason(req));
    amxc_var_add_key(amxc_ts_t, event_params, "StartTime", ts_start);
    amxc_var_add_key(amxc_ts_t, event_params, "CompleteTime", ts_end);
    amxc_var_add_key(cstring_t, event_params, "TransferURL", transfer_url);

    if(error_code != ftx_error_code_no_error) {
        SAH_TRACEZ_ERROR(ME, "error code: (%u) reason: %s", error_code, ftx_request_get_error_reason(req));
    } else {
        SAH_TRACEZ_INFO(ME, "file size: %u", ftx_request_get_file_size(req));
        SAH_TRACEZ_INFO(ME, "Upload Time Start : %s", time_start_str);
        SAH_TRACEZ_INFO(ME, "Upload Time End   : %s", time_end_str);
    }

    if(amxb_call(ctx, "LocalAgent.", "SendTransferComplete", event_params, ret, 5) != AMXB_STATUS_OK) {
        SAH_TRACEZ_WARNING(ME, "Transfer complete event could not be sent");
    }

    ftx_request_delete(&req);
    amxc_var_delete(&event_params);
    free(transfer_url);
    return true;
}

amxd_status_t _Upload(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {

    amxd_status_t status = amxd_status_unknown_error;
    ftx_request_t* request = NULL;
    char* file_path = NULL;
    const char* file_name = NULL;
    char* transfer_url = NULL;
    const char* url = GET_CHAR(args, "URL");
    const char* username = GET_CHAR(args, "Username");
    const char* password = GET_CHAR(args, "Password");

    when_false_trace((strlen(url) < 2048), fail, ERROR, "URL MaximumSize is 2048");
    when_false_trace((strlen(username) < 256), fail, ERROR, "Username MaximumSize is 256");
    when_false_trace((strlen(password) < 256), fail, ERROR, "Password MaximumSize is 256");
    transfer_url = strdup(url);

    file_path = get_filepath(object_const_char(object, "Name"));
    when_str_empty_trace(file_path, fail, ERROR, "There is no file path in the Name parameter");
    file_name = strrchr(file_path, '/');
    if(file_name != NULL) {
        file_name++;
    } else {
        file_name = file_path;
    }

    when_failed_trace(ftx_request_new(&request, ftx_request_type_upload, upload_cb), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_url(request, url), fail, ERROR, "URL does not set.");
    when_failed_trace(ftx_request_set_credentials(request, ftx_authentication_any, username, password), fail, ERROR, "Credentials Error.");
    when_failed_trace(ftx_request_set_target_file(request, file_path), fail, ERROR, "There is no such file.");
    when_failed_trace(ftx_request_set_max_transfer_time(request, 600), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_data(request, (void*) transfer_url), fail, ERROR, "Could not set userdata.");
    when_failed_trace(ftx_request_set_upload_method(request, ftx_upload_method_post), fail, ERROR, "Could not set HTTP method.");
    when_failed_trace(ftx_request_set_http_multipart_form_data(request, "file", file_name, "application/octet-stream"), fail, ERROR, "Could not set HTTP multipart.");

    if(strstr(url, "https://") != NULL) {
        int rc = -1;
        const char* ca_cert = NULL;

        ca_cert = object_const_char(deviceinfo_get_root(), "CACertificate");
        when_str_empty_trace(ca_cert, fail, ERROR, "CA Certificate is not defined");

        rc = ftx_request_set_ca_certificates(request, ca_cert, NULL);
        when_failed_trace(rc, fail, ERROR, "Could not set CA Certificate");
    }

    when_failed_trace(ftx_request_send(request), fail, ERROR, "The send request failed.");

    status = amxd_status_ok;
    free(file_path);
    return status;

fail:
    free(file_path);
    free(transfer_url);
    ftx_request_delete(&request);
    return status;
}
