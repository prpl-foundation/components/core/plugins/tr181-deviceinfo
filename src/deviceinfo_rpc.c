/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo_rpc.h"
#include "deviceinfo_logrotate.h"
#include "deviceinfo.h"

#define CLONE_FILE_PATH "/etc/config/clone_macaddress"
#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'

static bool directory_exist(const char* path) {
    struct stat path_stat;
    bool exist = false;
    memset(&path_stat, 0, sizeof(struct stat));

    when_str_empty(path, exit);
    when_true((-1 == stat(path, &path_stat)), exit);

    exist = S_ISDIR(path_stat.st_mode);

exit:
    return exist;
}

static bool file_exist(const char* filename) {
    struct stat file_stat;
    bool exist = false;
    memset(&file_stat, 0, sizeof(struct stat));

    when_str_empty(filename, exit);
    exist = (stat(filename, &file_stat) != -1);

exit:
    return exist;
}

static void remove_log_file(const char* alias) {
    char* file_path = NULL;
    amxd_object_t* vendor_obj = NULL;

    when_str_empty_trace(alias, exit, ERROR, "Alias of the VendorLogFile is not provided");

    vendor_obj = amxd_dm_findf(deviceinfo_get_dm(), "DeviceInfo.VendorLogFile.%s", alias);
    when_null_trace(vendor_obj, exit, ERROR, "Could not find VendorLogFile.[Alias == %s]", alias);

    file_path = get_filepath(object_const_char(vendor_obj, "Name"));
    when_str_empty_trace(file_path, exit, ERROR, "Could not retrieve the file_path of VendorLogFile.[%s]", alias);
    when_false_trace(file_exist(file_path), exit, WARNING, "File does not exist : %s", file_path);

    remove(file_path);
    SAH_TRACEZ_INFO(ME, "File has been removed: %s", file_path);

exit:
    free(file_path);
    return;
}

static int copyFile(const char* sourceFilePath, const char* destinationFilePath) {
    FILE* sourceFile = fopen(sourceFilePath, "rb");
    FILE* destinationFile = fopen(destinationFilePath, "w");
    bool rc = -1;
    int ch;

    when_null_trace(sourceFile, exit, ERROR, "Error opening file: %s", sourceFilePath);
    when_null_trace(destinationFile, exit, ERROR, "Error opening file: %s", destinationFilePath);

    while((ch = fgetc(sourceFile)) != EOF) {
        fputc(ch, destinationFile);
    }

    rc = 0;
exit:
    if(sourceFile != NULL) {
        fclose(sourceFile);
    }
    if(destinationFile != NULL) {
        fclose(destinationFile);
    }
    return rc;
}

static bool delete_files_of_dir(const char* directory) {
    bool ret = false;
    struct dirent* entry;
    DIR* dir = NULL;
    amxc_string_t file_path;
    amxc_string_init(&file_path, 0);
    when_str_empty_trace(directory, exit, ERROR, "Invalid argument");

    dir = opendir(directory);
    when_null_trace(dir, exit, ERROR, "Failed to open directory: %s", directory);

    while((entry = readdir(dir)) != NULL) {
        if((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0)) {
            continue;
        }
        amxc_string_setf(&file_path, "%s/%s", directory, entry->d_name);
        if(remove(amxc_string_get(&file_path, 0)) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete file: %s", amxc_string_get(&file_path, 0));
        }
    }
    ret = true;

exit:
    if(dir != NULL) {
        closedir(dir);
    }
    amxc_string_clean(&file_path);
    return ret;

}

static void dm_set_state_of(amxd_object_t* object, const char* parameter, const char* state) {
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    when_null_trace(object, exit, ERROR, "The object was not given");
    when_str_empty_trace(parameter, exit, ERROR, "The parameter was not given");
    when_str_empty_trace(state, exit, ERROR, "The state was not given");

    amxd_trans_select_object(&transaction, object);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &transaction, parameter, state);
    if(amxd_status_ok != amxd_trans_apply(&transaction, deviceinfo_get_dm())) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction %s -> %s", parameter, state);
    }

exit:
    amxd_trans_clean(&transaction);
    return;
}

static void backup_call_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                             amxb_request_t* req,
                             int status,
                             void* priv) {
    amxc_string_t download_path;
    amxc_string_t backup_path;
    char* filename = (char*) priv;
    const char* backup_path_str = NULL;
    const char* download_path_str = NULL;
    int ret = -1;

    amxc_string_init(&backup_path, 0);
    amxc_string_init(&download_path, 0);

    when_failed_trace(status, exit, ERROR, "Call Backup failed with status %d - %s",
                      status, amxd_status_string((amxd_status_t) status));

    amxc_string_setf(&download_path, "%s/%s", DOWNLOAD_DIR, filename);
    amxc_string_setf(&backup_path, "%s/%s", BACKUP_DIR, GETP_CHAR(req->result, "0.FileName"));
    download_path_str = amxc_string_get(&download_path, 0);
    backup_path_str = amxc_string_get(&backup_path, 0);

    when_false_trace(file_exist(backup_path_str), exit, ERROR, "Backup file: %s does not exist", backup_path_str);
    when_false_trace(directory_exist(DOWNLOAD_DIR), exit, ERROR, "Download directory: %s does not exist", DOWNLOAD_DIR);

    delete_files_of_dir(DOWNLOAD_DIR);

    ret = copyFile(backup_path_str, download_path_str);

exit:
    dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", (ret == 0) ? "Success" : "Error");
    amxc_string_clean(&backup_path);
    amxc_string_clean(&download_path);
    free(priv);
    amxb_close_request(&req);
}

static void restore_call_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                              amxb_request_t* req,
                              int status,
                              UNUSED void* priv) {
    dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", (status == 0) ? "Success" : "Error");
    when_failed_trace(status, exit, ERROR, "Call Restore failed with status %d - %s",
                      status, amxd_status_string((amxd_status_t) status));
    SAH_TRACEZ_INFO(ME, "Restore callback succeed %d", status);
exit:
    amxb_close_request(&req);
}

amxd_status_t _WriteUsersetting(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_string_t upload_file;
    amxc_string_t backup_file;
    amxb_bus_ctx_t* pcm_ctx = NULL;
    amxc_var_t restore_args;
    amxc_var_t add_backup_ret;
    amxc_var_t add_backup_arg;
    const char* alias = NULL;
    const char* filename = NULL;
    amxb_request_t* request = 0;
    const char* upload_file_str = NULL;
    const char* backup_file_str = NULL;

    amxc_string_init(&upload_file, 0);
    amxc_string_init(&backup_file, 0);
    amxc_var_init(&restore_args);
    amxc_var_init(&add_backup_ret);
    amxc_var_init(&add_backup_arg);

    dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", "Ongoing");

    when_null_trace(args, exit, ERROR, "Invalid argument");
    amxc_string_setf(&upload_file, "%s/%s", UPLOAD_DIR, GET_CHAR(args, "FileName"));
    upload_file_str = amxc_string_get(&upload_file, 0);
    when_false_trace(file_exist(upload_file_str), exit, ERROR, "Upload file: %s does not exist", upload_file_str);

    amxc_var_set_type(&add_backup_arg, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &add_backup_arg, "Tag", "Upload");
    amxc_var_add_key(cstring_t, &add_backup_arg, "FileName", GET_CHAR(args, "FileName"));

    pcm_ctx = amxb_be_who_has(PCM_OBJECT);
    when_null_trace(pcm_ctx, exit, ERROR, "Bus for %s not found", PCM_OBJECT);

    if(amxb_call(pcm_ctx, PCM_OBJECT, "AddBackupFile", &add_backup_arg, &add_backup_ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Unable to add new instance to the BackupFile");
        goto exit;
    }
    alias = GETP_CHAR(&add_backup_ret, "0.Alias");
    filename = GETP_CHAR(&add_backup_ret, "0.FileName");

    when_str_empty_trace(alias, exit, ERROR, "Alias is empty");
    when_str_empty_trace(filename, exit, ERROR, "Filename is empty");

    amxc_string_setf(&backup_file, "%s/%s", BACKUP_DIR, filename);
    backup_file_str = amxc_string_get(&backup_file, 0);
    when_false_trace(directory_exist(BACKUP_DIR), exit, ERROR, "Backup directory: %s does not exist", BACKUP_DIR);

    //Upload backup file should be moved anyway from the upload directory /tmp/upload.
    when_failed_trace(copyFile(upload_file_str, backup_file_str),
                      exit, ERROR, "Unable to copy the backup file to %s", DOWNLOAD_DIR);

    remove(upload_file_str);

    amxc_var_set_type(&restore_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &restore_args, "Type", "export");
    amxc_var_add_key(cstring_t, &restore_args, "FileRef", alias);

    request = amxb_async_call(pcm_ctx, PCM_OBJECT, "Restore", &restore_args, restore_call_done, NULL);
    if(request == NULL) {
        SAH_TRACEZ_ERROR("Failed to call [Restore] of [%s]", PCM_OBJECT);
        goto exit;
    }
    status = amxd_status_ok;

exit:
    if(status != amxd_status_ok) {
        dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", "Error");
    }
    amxc_string_clean(&upload_file);
    amxc_string_clean(&backup_file);
    amxc_var_clean(&restore_args);
    amxc_var_clean(&add_backup_ret);
    amxc_var_clean(&add_backup_arg);
    return status;

}

amxd_status_t _ReadUsersetting(UNUSED amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxb_bus_ctx_t* pcm_ctx = NULL;
    amxc_var_t backup_args;
    amxb_request_t* request = 0;
    char* filename = NULL;

    dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", "Ongoing");

    amxc_var_init(&backup_args);
    when_null_trace(args, exit, ERROR, "Invalid argument");
    filename = amxc_var_dyncast(cstring_t, GET_ARG(args, "FileName"));
    if(STR_EMPTY(filename)) {
        SAH_TRACEZ_ERROR(ME, "FileName argument is not provided");
        free(filename);
        goto exit;
    }

    amxc_var_set_type(&backup_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &backup_args, "Type", "export");
    amxc_var_add_key(cstring_t, &backup_args, "Tag", "Manual");

    pcm_ctx = amxb_be_who_has(PCM_OBJECT);
    if(pcm_ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "BUS for %s not found", PCM_OBJECT);
        free(filename);
        goto exit;
    }
    request = amxb_async_call(pcm_ctx, PCM_OBJECT, "Backup", &backup_args, backup_call_done, filename);
    if(request == NULL) {
        SAH_TRACEZ_ERROR("Failed to call [Backup] of [%s]", PCM_OBJECT);
        goto exit;
    }
    status = amxd_status_ok;

exit:
    if(status != amxd_status_ok) {
        dm_set_state_of(deviceinfo_get_root(), "UsersettingStatus", "Error");
    }
    amxc_var_clean(&backup_args);
    return status;
}

// Function to check if a string is a valid MAC address
static int is_Valid_MacAddress(const char* macAddress) {
    int count = 0;

    // Iterate through each character in the MAC address
    for(size_t i = 0; i < strlen(macAddress); i++) {
        // Check if the character is a valid hexadecimal digit
        if(isxdigit(macAddress[i])) {
            count++;
            // Check if the count exceeds the valid MAC address length
            if(count > 12) {
                return -1; // Not a valid MAC address
            }
        } else if((macAddress[i] == ':') || (macAddress[i] == '-')) {
            // Skip valid separators
        } else {
            return -1; // Not a valid MAC address
        }
    }

    return (!(count == 12)); // Return true if the count is exactly 12
}

// Function to write a MAC address to a file
static int write_MacAddress_To_File(const char* macAddress) {
    FILE* file = fopen(CLONE_FILE_PATH, "w");

    if(file != NULL) {
        fprintf(file, "MACADDRESS=%s\n", macAddress);
        fclose(file);
        return 0; // Success
    } else {
        SAH_TRACEZ_ERROR("Error: Unable to open file %s for writing.\n", CLONE_FILE_PATH);
        return -1; // Failure
    }
}

static void set_clonning_macaddress(const char* value) {
    amxc_string_t parameter;
    amxc_string_init(&parameter, 0);
    amxc_string_setf(&parameter, "%sClonedMACAddress",
                     vendor_prefix());
    deviceinfo_set_on_dm(deviceinfo_get_root(), amxc_string_get(&parameter, 0), value);
    amxc_string_clean(&parameter);
}

amxd_status_t _CloneMacAddress(UNUSED amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {

    const char* macAddress = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int retval = -1;

    when_null_trace(args, exit, ERROR, "Invalid argument");

    macAddress = GET_CHAR(args, "MACAddress");

    retval = is_Valid_MacAddress(macAddress);

    when_failed_trace(retval, exit, ERROR, "%s is NOT a valid MAC address.", macAddress);

    retval = write_MacAddress_To_File(macAddress);

    when_failed_trace(retval, exit, ERROR, "Operation failed.\n");

    set_clonning_macaddress(macAddress);

    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _RestoreMacAddress(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {

    amxd_status_t status = amxd_status_ok;

    // Check if the file exists
    if((remove(CLONE_FILE_PATH) != 0) && (errno != ENOENT)) {
        SAH_TRACEZ_WARNING("Error removing file %s \n", CLONE_FILE_PATH);
        status = amxd_status_unknown_error;
    }
    set_clonning_macaddress("");
    return status;
}

amxd_status_t _SetVendorLogFile(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* alias = GET_CHAR(args, "Alias");
    amxc_var_t* data = GET_ARG(args, "Data");
    const char* name = GET_CHAR(data, "Name");
    char* current_name = NULL;
    amxd_object_t* vendor_obj = NULL;
    amxd_trans_t transaction;
    amxc_string_t file_path;

    amxc_string_init(&file_path, 0);
    amxd_trans_init(&transaction);

    when_str_empty_trace(alias, exit, ERROR, "Alias is not provided");
    when_null_trace(data, exit, ERROR, "Data is not provided");
    when_str_empty_trace(name, exit, ERROR, "Name parameter is not provided");

    amxc_string_setf(&file_path, "file://%s", name);
    vendor_obj = amxd_dm_findf(deviceinfo_get_dm(), "DeviceInfo.VendorLogFile.%s", alias);

    if(vendor_obj == NULL) {
        amxd_trans_select_pathf(&transaction, "DeviceInfo.VendorLogFile.");
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
        amxd_trans_add_inst(&transaction, 0, alias);
    } else {
        amxd_trans_select_object(&transaction, vendor_obj);
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    }

    current_name = get_filepath(object_const_char(vendor_obj, "Name"));
    if((current_name != NULL) && (strcmp(name, current_name) != 0)) {
        remove_log_file(alias);
    }

    amxd_trans_set_value(int32_t, &transaction, "MaximumSize", GET_INT32(data, "MaximumSize"));
    amxd_trans_set_value(cstring_t, &transaction, "Name", amxc_string_get(&file_path, 0));
    amxd_trans_set_value(bool, &transaction, "Persistent", GET_BOOL(data, "Persistent"));

    status = amxd_trans_apply(&transaction, deviceinfo_get_dm());
    when_failed_trace(status, exit, ERROR, "Transaction failed with %d", status);

exit:
    free(current_name);
    amxc_string_clean(&file_path);
    amxd_trans_clean(&transaction);
    return status;
}