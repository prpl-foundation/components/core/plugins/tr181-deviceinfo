/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object_event.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo_firmwareImage.h"
#include "deviceinfo_dm_mngr.h"
#include "deviceinfo.h"


#define MAX_RETRIES_TIMER 20


static amxp_timer_t* timer_fw_info_1 = NULL;
static amxp_timer_t* timer_fw_info_2 = NULL;
static int counter_timer_1 = 0;
static int counter_timer_2 = 0;

static void retry_fw_get_info(UNUSED amxp_timer_t* timer, void* data) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* fwi_obj = (amxd_object_t*) data;
    int32_t fw_index = 0;

    when_null(fwi_obj, exit);
    fw_index = (int32_t) amxd_object_get_index(fwi_obj);
    when_false(fw_index > 0, exit);
    firmware_image_get_info(fwi_obj, fw_index);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _Activate(UNUSED amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* params = NULL;
    amxc_var_t data;
    const char* alias = object_const_char(object, "Alias");
    char* object_path = NULL;
    const char* controller = GET_CHAR(amxd_object_get_param_value(object, "Controller"), NULL);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    when_null(alias, exit);

    if(strncmp(alias, "active", 6) == 0) {
        SAH_TRACEZ_ERROR(ME, "Can not activate already active image.");
        status = amxd_status_invalid_function;
        goto exit;
    }

    when_failed_trace(deviceinfo_set_firmware_controller_parser_and_dm(controller), exit, ERROR,
                      "Failed to set parser & dm for firmware controller");

    object_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);

    amxc_var_add_key(uint32_t, &data, "fwi-id", amxd_object_get_index(object));
    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    amxc_var_add_key(cstring_t, &data, "alias", alias);
    amxc_var_add_key(cstring_t, &data, "object", object_path);
    amxc_var_copy(params, args);

    if(amxm_execute_function(controller, "firmware-ctrl", "activate", &data, ret) != 0) {
        status = amxd_status_invalid_function;
        SAH_TRACEZ_ERROR(ME, "Could not activate firmware image. Activation aborted.");
        goto exit;
    }
    status = amxd_status_ok;
exit:
    free(object_path);
    amxc_var_clean(&data);
    return status;
}

amxd_status_t _Download(amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* params = NULL;
    amxc_var_t data;
    const char* alias = object_const_char(object, "Alias");
    char* object_path = NULL;
    const char* ca_certificate = NULL;
    amxc_var_t fwi_params;
    const char* controller = GET_CHAR(amxd_object_get_param_value(object, "Controller"), NULL);

    amxc_var_init(&data);
    amxc_var_init(&fwi_params);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    when_null(alias, exit);

    if((strcmp(alias, "active") == 0) && !GET_BOOL(args, "AutoActivate")) {
        SAH_TRACEZ_ERROR(ME, "AutoActivate must be true on active firmwareimage. Download aborted.");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }
    if((strcmp(alias, "inactive") == 0) && GET_BOOL(args, "AutoActivate")) {
        SAH_TRACEZ_ERROR(ME, "AutoActivate must be false on inactive firmwareimage. Download aborted.");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    when_failed_trace(deviceinfo_set_firmware_controller_parser_and_dm(controller), exit, ERROR,
                      "Failed to set parser & dm for firmware controller");

    amxd_object_get_params(object, &fwi_params, amxd_dm_access_protected);
    ca_certificate = GET_CHAR(&fwi_params, "CACertificate");
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "fwi-id", amxd_object_get_index(object));
    amxc_var_add_key(cstring_t, &data, "AuthenticationType", GET_CHAR(&fwi_params, "AuthenticationType"));
    amxc_var_add_key(cstring_t, &data, "CACertificate", ca_certificate);
    amxc_var_add_key(cstring_t, &data, "TLSEngine", GET_CHAR(&fwi_params, "TLSEngine"));
    amxc_var_add_key(cstring_t, &data, "TLSClientCertificate", GET_CHAR(&fwi_params, "TLSClientCertificate"));
    amxc_var_add_key(cstring_t, &data, "TLSPrivateKey", GET_CHAR(&fwi_params, "TLSPrivateKey"));
    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    amxc_var_add_key(cstring_t, &data, "alias", alias);
    object_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);
    amxc_var_add_key(cstring_t, &data, "object", object_path);
    amxc_var_copy(params, args);

    if(amxm_execute_function(controller, "firmware-ctrl", "download", &data, ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "Internal error when calling module. Download aborted.");
        status = amxd_status_invalid_function;
        goto exit;
    }
    status = amxd_status_ok;
exit:
    free(object_path);
    amxc_var_clean(&data);
    amxc_var_clean(&fwi_params);
    return status;
}

void _firmware_image_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    amxd_object_t* tmpl_fwi = amxd_dm_signal_get_object(deviceinfo_get_dm(), event_data);
    amxd_object_t* obj_fwi = amxd_object_get_instance(tmpl_fwi, NULL, GET_UINT32(event_data, "index"));

    SAH_TRACEZ_INFO(ME, "Received event: %s", sig_name);

    firmware_image_get_info(obj_fwi, GET_INT32(event_data, "index"));

    return;
}

static const cstring_t get_string_param(const char* parameter, bool with_prefix) {
    amxd_object_t* deviceinfo = amxd_dm_findf(deviceinfo_get_dm(), "DeviceInfo.");
    amxc_string_t param;
    const cstring_t value = NULL;

    amxc_string_init(&param, 0);

    when_null(deviceinfo, exit);
    amxc_string_setf(&param, "%s%s", with_prefix ? vendor_prefix() : "", parameter);
    value = GET_CHAR(amxd_object_get_param_value(deviceinfo, amxc_string_get(&param, 0)), NULL);

exit:
    amxc_string_clean(&param);
    return value;
}

int firmware_image_get_info(amxd_object_t* object, int32_t index) {
    amxc_var_t data;
    amxc_var_t retdata;
    int status = -1;
    int rv = -1;
    bool retried = false;
    amxd_object_t* fwi_obj = NULL;
    int32_t fw_index = -1;
    amxp_timer_t** timer = NULL;
    int* counter_timer = NULL;
    const char* alias = object_const_char(object, "Alias");
    const char* controller = NULL;
    const char* software_version = NULL;
    amxd_object_t* deviceinfo = amxd_dm_findf(deviceinfo_get_dm(), "DeviceInfo.");

    amxc_var_init(&data);
    amxc_var_init(&retdata);

    when_null(deviceinfo, exit);

    if((object == NULL) || (amxd_object_instance != amxd_object_get_type(object))) {
        if(index > 0) {
            fwi_obj = amxd_dm_findf(deviceinfo_get_dm(), "%sDeviceInfo.FirmwareImage.%s.", (prefix_() != NULL) ? prefix_() : "", (index == 1) ? "active" : "inactive");
        }
    } else {
        fwi_obj = object;
    }
    when_null(fwi_obj, exit);

    if(index <= 0) {
        fw_index = (int32_t) amxd_object_get_index(fwi_obj);
    } else {
        fw_index = index;
    }
    when_false_trace(fw_index > 0, exit, ERROR, "FirmwareImage index is not a positive value.");

    if(fw_index == 1) {
        timer = &timer_fw_info_1;
        counter_timer = &counter_timer_1;
    } else {
        timer = &timer_fw_info_2;
        counter_timer = &counter_timer_2;
    }

    controller = GET_CHAR(amxd_object_get_param_value(fwi_obj, "Controller"), NULL);

    when_failed_trace(deviceinfo_set_firmware_controller_parser_and_dm(controller), exit, ERROR,
                      "Failed to set parser & dm for firmware controller");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "alias", alias);
    amxc_var_add_key(int32_t, &data, "index", fw_index);
    amxc_var_set_type(&retdata, AMXC_VAR_ID_HTABLE);

    status = amxm_execute_function(controller, "firmware-ctrl", "info", &data, &retdata);
    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Can't execute GetFirmwareInfo in module");
    }

    when_str_empty(GET_CHAR(&retdata, "version"), retry);
    when_str_empty(GET_CHAR(&retdata, "name"), retry);

    software_version = get_string_param("SoftwareVersion", false);
    if(fw_index == 1) { //Make the DeviceInfo.FirmwareImage.1.Version the same as DeviceInfo.SoftwareUpdate
        if(STRING_EMPTY(software_version)) {
            deviceinfo_set_on_dm(deviceinfo, "SoftwareVersion", GET_CHAR(&retdata, "version"));
            deviceinfo_set_on_dm(fwi_obj, "Version", GET_CHAR(&retdata, "version"));
        } else {
            deviceinfo_set_on_dm(deviceinfo, "SoftwareVersion", software_version);
            deviceinfo_set_on_dm(fwi_obj, "Version", software_version);
        }
    } else {
        deviceinfo_set_on_dm(fwi_obj, "Version", GET_CHAR(&retdata, "version"));
    }

    deviceinfo_set_on_dm(fwi_obj, "Name", GET_CHAR(&retdata, "name"));

    rv = 0;
retry:
    when_true(rv == 0, exit);
    when_null(timer, exit);
    when_null(counter_timer, exit);
    when_null(fwi_obj, exit);

    if(*timer == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to get the Firmwareimage.%d info from the device block, retrying in 3 seconds", fw_index);
        amxp_timer_new(timer, retry_fw_get_info, fwi_obj);
        *counter_timer = 0;
    }
    if(*counter_timer >= MAX_RETRIES_TIMER) {
        SAH_TRACEZ_ERROR(ME, "Failed to get the Firmwareimage.%d info from the device block after %d retries", fw_index, MAX_RETRIES_TIMER);
    } else {
        SAH_TRACEZ_INFO(ME, "Failed to get the Firmwareimage.%d info from the device block, retrying again in 3 seconds", fw_index);
        amxp_timer_start(*timer, 3000);
        (*counter_timer)++;
        retried = true;
    }
exit:
    if(!retried) {
        amxp_timer_delete(timer);
        if(timer != NULL) {
            *timer = NULL;
        }
        if(counter_timer != NULL) {
            *counter_timer = 0;
        }
    }
    amxc_var_clean(&retdata);
    amxc_var_clean(&data);

    return rv;
}
