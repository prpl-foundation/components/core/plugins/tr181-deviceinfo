/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <sys/utsname.h>

#include "deviceinfo.h"
#include "dm_process_info.h"

#include "deviceinfo_dm_mngr.h"
#include "deviceinfo_vendor_log_file.h"
#include "deviceinfo_firmwareImage.h"
#include "deviceinfo_logrotate.h"
#include "filetransfer/filetransfer.h"
#include "deviceinfo_tmpfs.h"

static deviceinfo_app_t app;
static amxd_object_t* root_obj;

extern amxd_status_t get_processors(amxd_dm_t* dm);
extern amxd_status_t create_config_log_file(amxd_dm_t* dm);

static amxd_status_t deviceinfo_set_busctx(const char* root_string) {
    amxd_status_t status = amxd_status_unknown_error;
    app.bus_ctx = amxb_be_who_has(root_string);
    if(app.bus_ctx != NULL) {
        status = amxd_status_ok;
    }
    return status;
}

static void filetransfer_event_cb(int fd, UNUSED void* priv) {
    ftx_fd_event_handler(fd);
}

static int filetransfer_fdset_cb(int fd, bool add) {
    int retval = -1;
    if(add) {
        retval = amxo_connection_add(deviceinfo_get_parser(), fd, filetransfer_event_cb, NULL, AMXO_LISTEN, NULL);
    } else {
        retval = amxo_connection_remove(deviceinfo_get_parser(), fd);
    }
    SAH_TRACEZ_INFO(ME, "%s connection fd %d %s", add ? "add" : "remove", fd, (retval == 0) ? "success" : "failure");
    return retval;
}

static void set_parameter_from_environment(const char* parameter, const char* env_var) {
    deviceinfo_set_on_dm(deviceinfo_get_root(), parameter, getenv(env_var));
}

static int deviceinfo_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxc_string_t root;
    const char* prefix = amxc_var_constcast(cstring_t, amxo_parser_get_config(parser, "prefix_"));
    int rv = -1;

    SAH_TRACEZ_INFO(ME, "**************************************");
    SAH_TRACEZ_INFO(ME, "*        DeviceInfo started          *");
    SAH_TRACEZ_INFO(ME, "**************************************");

    if(prefix == NULL) {
        prefix = "";
    }

    amxc_string_init(&root, 0);
    amxc_string_setf(&root, "%sDeviceInfo", prefix);

    app.dm = dm;
    app.parser = parser;
    deviceinfo_set_busctx(amxc_string_get(&root, 0));

    if(get_processors(dm) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to get information about processors");
    }
    amxd_object_t* firmwareimage_object;

    root_obj = amxd_dm_findf(dm, "%s", amxc_string_get(&root, 0));
    amxc_string_clean(&root);

    firmwareimage_object = amxd_object_get_child(root_obj, "FirmwareImage");

    process_dm_add_objects();
    ftx_init(filetransfer_fdset_cb);

    set_parameter_from_environment("HardwareVersion", "HARDWAREVERSION");
    set_parameter_from_environment("SoftwareVersion", "SOFTWAREVERSION");

    when_failed(deviceinfo_dm_mngr_init(), exit);
    amxd_object_for_each(instance, it, firmwareimage_object) {
        firmware_image_get_info(amxc_container_of(it, amxd_object_t, it), -1);
    }

    process_refresh_init();

    //Create tmpfs
    tmpfs_partition_init();

    rv = 0;
exit:
    return rv;
}

static void deviceinfo_clean(void) {
    process_refresh_clean();
    ftx_clean();
    deviceinfo_dm_mngr_clean();
    app.dm = NULL;
    app.parser = NULL;
}

amxd_dm_t* deviceinfo_get_dm(void) {
    return app.dm;
}

amxo_parser_t* deviceinfo_get_parser(void) {
    return app.parser;
}

amxb_bus_ctx_t* deviceinfo_get_busctx(void) {
    return app.bus_ctx;
}

amxc_var_t* deviceinfo_get_config(void) {
    return &(app.parser->config);
}

amxd_object_t* deviceinfo_get_root(void) {
    return root_obj;
}

static const char* config_const_char(const char* key) {
    const char* value = GET_CHAR(amxo_parser_get_config(deviceinfo_get_parser(), key), NULL);
    return value != NULL ? value : "";
}

static int32_t config_int32(const char* key) {
    return GET_INT32(amxo_parser_get_config(deviceinfo_get_parser(), key), NULL);
}

const char* download_tmpfs_partition(void) {
    return config_const_char("download_tmpfs_partition");
}

uint32_t download_tmpfs_size_mb(void) {
    int32_t download_tmpfs_size_mb = config_int32("download_tmpfs_size_mb");
    return download_tmpfs_size_mb < 0 ? 0 :(uint32_t) download_tmpfs_size_mb;
}

const char* vendor_prefix(void) {
    return config_const_char("vendor_prefix");
}

const char* prefix_(void) {
    return config_const_char("prefix_");
}

const char* object_const_char(amxd_object_t* object, const char* name) {
    return GET_CHAR(amxd_object_get_param_value(object, name), NULL);
}

int deviceinfo_set_on_dm(amxd_object_t* obj, const cstring_t key, const cstring_t value) {
    int rv = -1;
    amxc_var_t* tmp_var = NULL;
    amxd_trans_t* trans = NULL;

    when_null(obj, exit);
    when_failed(amxd_trans_new(&trans), exit);
    when_failed(amxd_trans_set_attr(trans, amxd_tattr_change_ro, true), exit);
    when_failed(amxd_trans_select_object(trans, obj), exit);
    amxc_var_new(&tmp_var);
    amxc_var_set(cstring_t, tmp_var, value);
    amxd_trans_set_param(trans, key, tmp_var);
    when_failed(amxd_trans_apply(trans, deviceinfo_get_dm()), exit);

    rv = 0;
exit:
    amxd_trans_delete(&trans);
    amxc_var_delete(&tmp_var);
    return rv;
}

int _deviceinfo_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {

    int retval = 0;

    SAH_TRACEZ_INFO(ME, "deviceinfo_main, reason: %i", reason);
    switch(reason) {
    case AMXO_START:
        retval = deviceinfo_init(dm, parser);
        break;
    case AMXO_STOP:
        deviceinfo_clean();
        break;
    default:
        retval = -1;
        break;
    }

    return retval;
}

void _last_upgrade_changed(UNUSED const char* const sig_name,
                           UNUSED const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    const char* vendor_prefix = NULL;
    const char* last_upgrade_date = NULL;
    int ok = -1;
    amxc_ts_t tsp = {0};
    amxd_trans_t trans;
    amxc_string_t param_name;

    vendor_prefix = amxc_var_constcast(cstring_t, amxo_parser_get_config(deviceinfo_get_parser(), "vendor_prefix"));
    if(vendor_prefix == NULL) {
        vendor_prefix = "";
    }

    last_upgrade_date = getenv("LAST_UPGRADE_DATE");
    when_null_trace(last_upgrade_date, out, INFO, "Reboot without firmware upgrade");

    SAH_TRACEZ_INFO(ME, "Reboot after firmware upgrade: %s", last_upgrade_date);

    ok = amxc_ts_parse(&tsp, last_upgrade_date, strlen(last_upgrade_date));
    if(ok != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse timestamp '%s', fallback to now", last_upgrade_date);
        ok = amxc_ts_now(&tsp);
    }
    when_false_trace(ok == 0, out, ERROR, "Failed to update %sLastUpgradeDate", vendor_prefix);

    amxc_string_init(&param_name, 0);
    amxc_string_setf(&param_name, "%sLastUpgradeDate", vendor_prefix);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, deviceinfo_get_root());
    amxd_trans_set_value(amxc_ts_t, &trans, amxc_string_get(&param_name, 0), &tsp);
    amxd_trans_apply(&trans, deviceinfo_get_dm());

    amxd_trans_clean(&trans);
    amxc_string_clean(&param_name);

out:
    return;
}

void _syslog_start_sync(UNUSED const char* const sig_name,
                        UNUSED const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    amxc_var_t args;
    amxc_var_t ret;
    int retval = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    retval = amxm_execute_function(MOD_SYSLOG_ALIAS, MOD_SYSLOG_CTRL, "start-syslog-sync", &args, &ret);
    when_failed_trace(retval, exit, ERROR, "Could not start Syslog sync!");

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}