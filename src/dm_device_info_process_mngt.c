/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "cpu_info.h"
#include "deviceinfo.h"
#include "dm_process_info.h"

#undef ME
#define ME "devinfo-process"

static amxc_set_t current;
static amxp_timer_t* timer_process_refresh = NULL;

static uint64_t cpu_dm_calc_usage(cpu_usage_t* old, cpu_usage_t* cur) {
    uint64_t percentage = 0;
    uint64_t owj = old->user + old->nice + old->system;
    uint64_t otj = owj + old->idle + old->iowait + old->irq + old->softirq;

    uint64_t nwj = cur->user + cur->nice + cur->system;
    uint64_t ntj = nwj + cur->idle + cur->iowait + cur->irq + cur->softirq;
    percentage = ((nwj - owj) * 100);
    if((ntj - otj) != 0) {
        percentage /= (ntj - otj);
    } else {
        percentage = 999;
    }
    return percentage;
}

static cpu_usage_t* cpu_dm_get_prev_data(amxd_object_t* cpu_instance,
                                         amxd_param_t* usage) {
    cpu_usage_t* data = NULL;
    if(cpu_instance != NULL) {
        data = (cpu_usage_t*) cpu_instance->priv;
    } else {
        data = (cpu_usage_t*) usage->priv;
    }

    return data;
}

static cpu_usage_t* cpu_dm_allocate_data(amxd_object_t* cpu_instance,
                                         amxd_param_t* usage) {
    cpu_usage_t* data = (cpu_usage_t*) calloc(1, sizeof(cpu_usage_t));
    if(cpu_instance != NULL) {
        cpu_instance->priv = data;
    } else {
        usage->priv = data;
    }

    return data;
}

void process_dm_add_objects(void) {
    amxc_var_t process_data;
    amxd_dm_t* dm = deviceinfo_get_dm();
    amxd_object_t* cpu_templ = amxd_dm_findf(dm, "%sDeviceInfo.ProcessStatus.Process.", prefix_());
    amxd_trans_t transaction;

    amxc_set_init(&current, false);
    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxc_var_init(&process_data);
    process_info_read(&process_data);

    amxc_var_for_each(var, (&process_data)) {
        uint32_t id = amxc_var_dyncast(uint32_t, var);
        char* flag = amxc_var_dyncast(cstring_t, var);
        amxd_trans_select_object(&transaction, cpu_templ);
        amxd_trans_add_inst(&transaction, id, NULL);
        amxc_set_add_flag(&current, flag);
        free(flag);
    }

    amxd_trans_apply(&transaction, dm);
    amxc_var_clean(&process_data);
    amxd_trans_clean(&transaction);
}

// this function is called from within object & parameter
// action implementations and therefor it is limmited in the possibility
// to read/write objects and/or parameters.
uint64_t cpu_dm_get_stats(amxd_object_t* cpu_instance,
                          amxd_param_t* usage) {
    cpu_usage_t usage_data;
    cpu_usage_t* prev_udata = cpu_dm_get_prev_data(cpu_instance, usage);
    uint64_t percentage = 0;

    cpu_stats_read(&usage_data);
    if(prev_udata == NULL) {
        prev_udata = cpu_dm_allocate_data(cpu_instance, usage);
    } else {
        percentage = cpu_dm_calc_usage(prev_udata, &usage_data);
    }
    memcpy(prev_udata, &usage_data, sizeof(cpu_usage_t));
    return percentage;
}

static void update_current(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    amxc_set_t to_delete;
    amxc_set_t to_add;
    amxc_set_t new_current;
    amxc_var_t process_data;
    amxd_dm_t* dm = deviceinfo_get_dm();
    amxd_object_t* process_obj = amxd_dm_findf(dm, "%sDeviceInfo.ProcessStatus.Process.", prefix_());
    amxd_trans_t transaction;
    amxd_status_t status;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);

    amxc_set_init(&to_delete, false);
    amxc_set_init(&to_add, false);
    amxc_set_init(&new_current, false);

    amxc_var_init(&process_data);
    process_info_read(&process_data);

    amxc_var_for_each(var, (&process_data)) {
        char* flag_name = amxc_var_dyncast(cstring_t, var);
        amxc_set_add_flag(&new_current, flag_name);
        free(flag_name);
    }

    amxc_set_union(&to_delete, &current);
    amxc_set_subtract(&to_delete, &new_current);

    amxc_set_union(&to_add, &new_current);
    amxc_set_subtract(&to_add, &current);

    amxc_set_reset(&current);
    amxc_set_union(&current, &new_current);

    amxc_set_iterate(flag, &to_delete) {
        const char* flag_name = amxc_flag_get_value((amxc_flag_t*) flag);
        amxd_trans_select_object(&transaction, process_obj);
        amxd_trans_del_inst(&transaction, atoi(flag_name), NULL);
    }

    amxc_set_iterate(flag, &to_add) {
        const char* flag_name = amxc_flag_get_value((amxc_flag_t*) flag);
        amxd_trans_select_object(&transaction, process_obj);
        amxd_trans_add_inst(&transaction, atoi(flag_name), NULL);
    }

    status = amxd_trans_apply(&transaction, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to amxd_trans_apply with error %d", status);
    }

    amxd_trans_clean(&transaction);
    amxc_var_clean(&process_data);
    amxc_set_clean(&to_delete);
    amxc_set_clean(&to_add);
    amxc_set_clean(&new_current);
}

void process_refresh_init(void) {
    int delay = 5000;

    if(amxc_var_get_key(deviceinfo_get_config(), "process-refresh-delay", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        delay = GET_UINT32(deviceinfo_get_config(), "process-refresh-delay") * 1000;
    }

    amxp_timer_new(&timer_process_refresh, update_current, NULL);
    amxp_timer_set_interval(timer_process_refresh, delay);
    amxp_timer_start(timer_process_refresh, delay);
}

void process_refresh_clean(void) {
    amxp_timer_delete(&timer_process_refresh);
    amxc_set_clean(&current);
}