/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "deviceinfo_dm_mngr.h"
#include "deviceinfo_logrotate.h"
#include "deviceinfo_vendor_log_file.h"

#define LR_ADD_SCRIPT "/usr/lib/debuginfo/logrotate_add.sh"
#define LR_REMOVE_SCRIPT "/usr/lib/debuginfo/logrotate_remove.sh"

static int dm_update_status(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    int retval = 0;
    amxb_bus_ctx_t* ctx = deviceinfo_get_busctx();
    amxc_var_t* status_var = amxc_var_take_key(args, "Status");
    amxc_var_t* fwi_path = amxc_var_take_key(args, "FwiPath");
    const char* status = NULL;
    amxc_var_t set;

    amxc_var_init(&set);
    if((status_var == NULL) || (fwi_path == NULL)) {
        SAH_TRACEZ_WARNING(ME, "FirmwareImage status can not be set correctly");
        goto exit;
    }

    status = amxc_var_constcast(cstring_t, status_var);
    amxc_var_set_type(&set, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &set, "Status", status);
    amxc_var_add_key(cstring_t, &set, "BootFailureLog", GET_CHAR(args, "FaultString"));

    if(amxb_set(ctx, amxc_var_constcast(cstring_t, fwi_path), &set, ret, 5) != 0) {
        SAH_TRACEZ_WARNING(ME, "FirmwareImage status can not be set correctly");
        goto exit;
    }

    if((strcmp(status, "Available") == 0) ||
       (strcmp(status, "ValidationFailed") == 0) ||
       (strcmp(status, "DownloadFailed") == 0) ||
       (strcmp(status, "InstallationFailed") == 0)) {
        if(amxb_call(ctx, "LocalAgent.", "SendTransferComplete", args, ret, 5) != AMXB_STATUS_OK) {
            SAH_TRACEZ_WARNING(ME, "Transfer complete event could not be sent");
            retval = -1;
        }
    }
exit:
    amxc_var_clean(&set);
    amxc_var_delete(&status_var);
    amxc_var_delete(&fwi_path);
    return retval;
}

static int get_syslog_config(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t config;

    when_null(deviceinfo_get_parser(), exit);
    config = deviceinfo_get_parser()->config;

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, args, "sync_vendorlog", amxc_var_get_key(&config, "sync-vendorlog-files", AMXC_VAR_FLAG_DEFAULT) == NULL ? true : GET_BOOL(&config, "sync-vendorlog-files"));
    amxc_var_add_key(bool, args, "sync_logrotate", amxc_var_get_key(&config, "sync-logrotate-files", AMXC_VAR_FLAG_DEFAULT) == NULL ? true : GET_BOOL(&config, "sync-logrotate-files"));
    retval = 0;

exit:
    return retval;
}

static int dm_mngr_register_func(void) {
    int ret = -1;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed(amxm_module_register(&mod, so, MOD_DM_MNGR), exit);
    ret = amxm_module_add_function(mod, "update-status", dm_update_status);
    ret |= amxm_module_add_function(mod, "update-logrotate-logfile", dm_update_logrotate_logfile);
    ret |= amxm_module_add_function(mod, "update-logrotate", dm_update_logrotate);
    ret |= amxm_module_add_function(mod, "update-vendorlogfile", dm_update_vendorlogfile);
    ret |= amxm_module_add_function(mod, "get-syslog-config", get_syslog_config);

exit:
    return ret;
}

static int dm_mngr_load_controllers(const char* controller_param, const char* so_alias) {
    int retval = -1;
    const amxc_var_t* controllers = amxd_object_get_param_value(deviceinfo_get_root(), controller_param);
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;
    amxm_shared_object_t* so = NULL;
    const char* const mod_dir = GET_CHAR(&(deviceinfo_get_parser()->config), "mod-dir");

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    when_null_trace(controllers, exit, ERROR, "Parameter %s not found", controller_param);
    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        retval = amxm_so_open(&so, so_alias != NULL ? so_alias : name, amxc_string_get(&mod_path, 0));
        if(retval == 0) {
            SAH_TRACEZ_INFO(ME, "Loading controller '%s' successful", name);
        } else {
            SAH_TRACEZ_ERROR(ME, "Loading controller '%s' failed", name);
        }
    }

    retval = 0; // will return ok if list of controllers is empty

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return retval;
}

static int dm_mngr_load_firmware_controllers(void) {
    return dm_mngr_load_controllers("SupportedControllers", NULL);
}

static int dm_mngr_load_logrotate_controllers(void) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t config = deviceinfo_get_parser()->config;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    retval = dm_mngr_load_controllers("LogRotateControllers", NULL);
    when_failed(retval, exit);

    retval = set_logrotate_interval();
    when_failed(retval, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "add", LR_ADD_SCRIPT);       // Script to add rotated files to the DM
    amxc_var_add_key(cstring_t, &args, "remove", LR_REMOVE_SCRIPT); // Script to check for files that can be removed
    amxc_var_add_key(cstring_t, &args, "firstaction", GET_CHAR(&config, "logrotate-firstaction-script") == NULL ? "" : GET_CHAR(&config, "logrotate-firstaction-script"));
    amxc_var_add_key(cstring_t, &args, "prerotate", GET_CHAR(&config, "logrotate-prerotate-script") == NULL ? "" : GET_CHAR(&config, "logrotate-prerotate-script"));
    amxc_var_add_key(cstring_t, &args, "postrotate", GET_CHAR(&config, "logrotate-postrotate-script") == NULL ? "" : GET_CHAR(&config, "logrotate-postrotate-script"));
    amxc_var_add_key(cstring_t, &args, "lastaction", GET_CHAR(&config, "logrotate-lastaction-script") == NULL ? "" : GET_CHAR(&config, "logrotate-lastaction-script"));
    amxc_var_add_key(bool, &args, "enable-add-script", amxc_var_get_key(&config, "logrotate-enable-add-script", AMXC_VAR_FLAG_DEFAULT) == NULL ? true : GET_BOOL(&config, "logrotate-enable-add-script"));
    retval = amxm_execute_function(MOD_LOGROTATE_ALIAS, MOD_LOGROTATE_CTRL, "set-scripts", &args, &ret);
    when_failed_trace(retval, exit, ERROR, "Could not set scripts for " MOD_LOGROTATE_ALIAS);

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return retval;
}

static int dm_mngr_load_syslog_controllers(void) {
    return dm_mngr_load_controllers("SyslogControllers", NULL);
}

int deviceinfo_set_firmware_controller_parser_and_dm(const char* controller) {
    int rv = -1;
    amxc_var_t ret;
    amxc_var_init(&ret);

    when_str_empty_trace(controller, exit, ERROR, "Empty controller name");

    when_failed_trace(amxm_execute_function(controller, "firmware-ctrl", "setparser",
                                            (amxc_var_t*) deviceinfo_get_parser(), &ret),
                      exit, ERROR, "Failed to set parser for controller '%s'", controller);
    when_failed_trace(amxm_execute_function(controller, "firmware-ctrl", "setdm",
                                            (amxc_var_t*) deviceinfo_get_dm(), &ret),
                      exit, ERROR, "Failed to set dm for controller '%s'", controller);

    rv = 0;

exit:
    amxc_var_clean(&ret);
    return rv;
}

int deviceinfo_dm_mngr_init(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Data model manager init");
    when_failed_trace(dm_mngr_register_func(), exit, ERROR, "Failed to register functions");
    when_failed_trace(dm_mngr_load_firmware_controllers(), exit, ERROR, "Failed to load firmware controllers");
    when_failed_trace(dm_mngr_load_logrotate_controllers(), exit, ERROR, "Failed to load logrotate controllers");
    when_failed_trace(dm_mngr_load_syslog_controllers(), exit, ERROR, "Failed to load syslog controllers");

    retval = 0;

exit:
    return retval;
}

void deviceinfo_dm_mngr_clean(void) {
    amxm_close_all();
}

amxd_status_t _call_ctrl(amxd_object_t* deviceinfo,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* ctrl_func = GET_ARG(args, "ctrl_func");
    const char* func_name = GET_CHAR(ctrl_func, NULL);
    amxc_var_take_it(ctrl_func);

    SAH_TRACEZ_INFO(ME, "Call controller function => '%s'", func_name);
    status = amxd_object_invoke_function(deviceinfo, func_name, args, ret);

    amxc_var_delete(&ctrl_func);

    return status;
}

void _controllers_reload(const char* const sig_name,
                         UNUSED const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    (void) sig_name; //  used variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Received event: %s", sig_name);

    when_failed_trace(dm_mngr_load_firmware_controllers(), exit, ERROR, "Failed to load firmware controllers");
    when_failed_trace(dm_mngr_load_logrotate_controllers(), exit, ERROR, "Failed to load logrotate controllers");
    when_failed_trace(dm_mngr_load_syslog_controllers(), exit, ERROR, "Failed to load syslog controllers");

exit:
    return;
}