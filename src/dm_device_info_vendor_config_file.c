/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo_vendor_config_file.h"
#include "deviceinfo_logrotate.h"
#include "filetransfer/filetransfer.h"
#include "deviceinfo.h"

static bool file_exists(const char* filename) {
    bool is_exist = false;
    when_failed_trace(access(filename, R_OK), exit, INFO, "Can't read file '%s': %s", filename, strerror(errno));
    is_exist = true;
exit:
    return is_exist;
}

static bool backup_cb(ftx_request_t* req, void* userdata) {
    amxc_var_t* req_data = (amxc_var_t*) userdata;
    amxb_bus_ctx_t* ctx = deviceinfo_get_busctx();
    amxc_ts_t* ts_end = NULL;
    amxc_ts_t* ts_start = NULL;
    ftx_error_code_t error_code;
    amxc_var_t* event_args = NULL;
    amxc_var_t values;
    int retval = -1;
    const char* transfer_url = GET_CHAR(req_data, "transfer_url");
    const char* object_path = GET_CHAR(req_data, "path");

    error_code = ftx_request_get_error_code(req);
    ts_end = ftx_request_get_end_time(req);
    ts_start = ftx_request_get_start_time(req);

    amxc_var_new(&event_args);
    amxc_var_set_type(event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, event_args, "TransferType", "Upload");
    amxc_var_add_key(uint32_t, event_args, "FaultCode", error_code);
    amxc_var_add_key(cstring_t, event_args, "FaultString", ftx_request_get_error_reason(req));
    amxc_var_add_key(amxc_ts_t, event_args, "StartTime", ts_start);
    amxc_var_add_key(amxc_ts_t, event_args, "CompleteTime", ts_end);
    amxc_var_add_key(cstring_t, event_args, "TransferURL", transfer_url);

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_ts_t, &values, "Date", ts_end);

    if(error_code != ftx_error_code_no_error) {
        SAH_TRACEZ_ERROR(ME, "error code: (%u) reason: %s", error_code, ftx_request_get_error_reason(req));
    } else {
        SAH_TRACEZ_INFO(ME, "Uploated File size: %u", ftx_request_get_file_size(req));
        retval = amxb_set(ctx, object_path, &values, NULL, 5);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "amxb_set failed with status: [%d] for path: [%s]", retval, object_path);
        }
    }

    if(amxb_call(ctx, "LocalAgent.", "SendTransferComplete", event_args, NULL, 5) != AMXB_STATUS_OK) {
        SAH_TRACEZ_WARNING(ME, "Transfer complete event could not be sent");
    }

    amxc_var_delete(&event_args);
    amxc_var_delete(&req_data);
    amxc_var_clean(&values);
    ftx_request_delete(&req);
    return true;
}

static bool restore_cb(ftx_request_t* req, void* userdata) {
    amxc_var_t* req_data = (amxc_var_t*) userdata;
    amxb_bus_ctx_t* ctx = deviceinfo_get_busctx();
    amxc_ts_t* ts_end = NULL;
    amxc_ts_t* ts_start = NULL;
    ftx_error_code_t error_code;
    amxc_var_t* event_args = NULL;
    amxc_var_t values;
    int retval = -1;

    const char* target_file = GET_CHAR(req_data, "target_file");
    const char* temp_target_file = GET_CHAR(req_data, "temp_target_file");
    const char* transfer_url = GET_CHAR(req_data, "transfer_url");
    const char* object_path = GET_CHAR(req_data, "path");

    error_code = ftx_request_get_error_code(req);
    ts_end = ftx_request_get_end_time(req);
    ts_start = ftx_request_get_start_time(req);

    amxc_var_new(&event_args);
    amxc_var_set_type(event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, event_args, "TransferType", "Download");
    amxc_var_add_key(uint32_t, event_args, "FaultCode", error_code);
    amxc_var_add_key(cstring_t, event_args, "FaultString", ftx_request_get_error_reason(req));
    amxc_var_add_key(amxc_ts_t, event_args, "StartTime", ts_start);
    amxc_var_add_key(amxc_ts_t, event_args, "CompleteTime", ts_end);
    amxc_var_add_key(cstring_t, event_args, "TransferURL", transfer_url);

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", target_file);

    if(error_code != ftx_error_code_no_error) {
        SAH_TRACEZ_ERROR(ME, "error code: (%u) reason: %s", error_code, ftx_request_get_error_reason(req));
        if(temp_target_file) {
            rename(temp_target_file, target_file);
        }
    } else {
        if(temp_target_file) {
            remove(temp_target_file);
        }
        SAH_TRACEZ_INFO(ME, "File downloaded - File size: %u", ftx_request_get_file_size(req));
    }

    retval = amxb_set(ctx, object_path, &values, NULL, 5);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "amxb_set failed with status: [%d] for path: [%s]", retval, object_path);
    }

    if(amxb_call(ctx, "LocalAgent.", "SendTransferComplete", event_args, NULL, 5) != AMXB_STATUS_OK) {
        SAH_TRACEZ_WARNING(ME, "Transfer complete event could not be sent");
    }

    amxc_var_delete(&event_args);
    amxc_var_delete(&req_data);
    amxc_var_clean(&values);
    ftx_request_delete(&req);
    return true;
}

amxd_status_t _Backup(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    ftx_request_t* request = NULL;
    char* file_path = NULL;
    const char* file_name = NULL;
    char* obj_path = NULL;
    const char* url = GET_CHAR(args, "URL");
    const char* username = GET_CHAR(args, "Username");
    const char* password = GET_CHAR(args, "Password");
    amxc_var_t* userdata = NULL;

    when_false_trace((strlen(url) < 2048), fail, ERROR, "URL MaximumSize is 2048");
    when_false_trace((strlen(username) < 256), fail, ERROR, "Username MaximumSize is 256");
    when_false_trace((strlen(password) < 256), fail, ERROR, "Password MaximumSize is 256");

    amxc_var_new(&userdata);
    amxc_var_set_type(userdata, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, userdata, "transfer_url", url);
    obj_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxc_var_add_key(cstring_t, userdata, "path", obj_path);

    file_path = get_filepath(object_const_char(object, "Name"));
    when_str_empty_trace(file_path, fail, ERROR, "There is no file path in the Name parameter");
    file_name = strrchr(file_path, '/');
    if(file_name != NULL) {
        file_name++;
    } else {
        file_name = file_path;
    }

    when_failed_trace(ftx_request_new(&request, ftx_request_type_upload, backup_cb), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_url(request, url), fail, ERROR, "URL does not set.");
    when_failed_trace(ftx_request_set_credentials(request, ftx_authentication_any, username, password), fail, ERROR, "Credentials Error.");
    when_failed_trace(ftx_request_set_target_file(request, file_path), fail, ERROR, "There is no such file.");
    when_failed_trace(ftx_request_set_max_transfer_time(request, 600), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_data(request, (void*) userdata), fail, ERROR, "Could not set userdata.");
    when_failed_trace(ftx_request_set_upload_method(request, ftx_upload_method_post), fail, ERROR, "Could not set HTTP method.");
    when_failed_trace(ftx_request_set_http_multipart_form_data(request, "file", file_name, "application/octet-stream"), fail, ERROR, "Could not set HTTP multipart.");

    if(strstr(url, "https://") != NULL) {
        int rc = -1;
        const char* ca_cert = NULL;

        ca_cert = object_const_char(deviceinfo_get_root(), "CACertificate");
        when_str_empty_trace(ca_cert, fail, ERROR, "CA Certificate is not defined");

        rc = ftx_request_set_ca_certificates(request, ca_cert, NULL);
        when_failed_trace(rc, fail, ERROR, "Could not set CA certificate");
    }

    when_failed_trace(ftx_request_send(request), fail, ERROR, "The send request failed.");

    status = amxd_status_ok;
    free(file_path);
    free(obj_path);
    return status;

fail:
    free(file_path);
    free(obj_path);
    amxc_var_delete(&userdata);
    ftx_request_delete(&request);
    return status;
}

amxd_status_t _Restore(UNUSED amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {

    amxd_status_t status = amxd_status_unknown_error;
    ftx_request_t* request = NULL;
    amxc_var_t* userdata = NULL;
    char* path = NULL;
    const char* url = GET_CHAR(args, "URL");
    const char* username = GET_CHAR(args, "Username");
    const char* password = GET_CHAR(args, "Password");
    const char* target_file = GET_CHAR(GET_ARG(args, "TargetFileName"), NULL);
    amxc_string_t temp_target_file;

    amxc_string_init(&temp_target_file, 0);

    when_false_trace((strlen(url) < 2048), fail, ERROR, "URL MaximumSize is 2048");
    when_false_trace((strlen(username) < 256), fail, ERROR, "Username MaximumSize is 256");
    when_false_trace((strlen(password) < 256), fail, ERROR, "Password MaximumSize is 256");

    if(target_file == NULL) {
        target_file = object_const_char(object, "Name");
    }

    if(file_exists(target_file)) {
        int rv = -1;
        amxc_string_setf(&temp_target_file, "%s.old", target_file);
        rv = rename(target_file, amxc_string_get(&temp_target_file, 0));
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "rename(%s) failed: %s", amxc_string_get(&temp_target_file, 0), strerror(errno));
        }
    }

    amxc_var_new(&userdata);
    amxc_var_set_type(userdata, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, userdata, "transfer_url", url);
    amxc_var_add_key(cstring_t, userdata, "temp_target_file", amxc_string_get(&temp_target_file, 0));
    amxc_var_add_key(cstring_t, userdata, "target_file", target_file);
    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxc_var_add_key(cstring_t, userdata, "path", path);

    when_failed_trace(ftx_request_new(&request, ftx_request_type_download, restore_cb), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_url(request, url), fail, ERROR, "URL does not set.");
    when_failed_trace(ftx_request_set_credentials(request, ftx_authentication_any, username, password), fail, ERROR, "Credentials Error.");
    when_failed_trace(ftx_request_set_target_file(request, target_file), fail, ERROR, "Empty target file name.");
    when_failed_trace(ftx_request_set_max_transfer_time(request, 600), fail, ERROR, "Could not allocate memory.");
    when_failed_trace(ftx_request_set_data(request, (void*) userdata), fail, ERROR, "Could not set userdata.");
    when_failed_trace(ftx_request_send(request), fail, ERROR, "The send request failed.");
    userdata = NULL; // I think ftx_request_delete doesn't free the userdata
    request = NULL;

    status = amxd_status_ok;

fail:
    amxc_string_clean(&temp_target_file);
    free(path);
    amxc_var_delete(&userdata);
    ftx_request_delete(&request);
    return status;
}
