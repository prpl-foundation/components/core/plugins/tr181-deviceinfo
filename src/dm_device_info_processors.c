/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <debug/sahtrace.h>
#include <sys/utsname.h>
#include "deviceinfo.h"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

amxd_status_t add_instances(amxd_trans_t* trans, char* architecture);
amxd_status_t get_processors(amxd_dm_t* dm);

/**********************************************************
* Functions
**********************************************************/

amxd_status_t add_instances(amxd_trans_t* trans, char* architecture) {

    amxd_status_t status = amxd_status_unknown_error;
    int processor_index = 1;
    char line[256] = {0};
    FILE* p_file = NULL;

    p_file = fopen("/proc/cpuinfo", "r");
    if(p_file == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to read /proc/cpuinfo");
        goto exit;
    }

    while(fgets(line, sizeof(line), p_file) != NULL) {

        if(strstr(line, "processor")) {
            status = amxd_trans_add_inst(trans, processor_index, NULL);
            if(status != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "Failed to create instance");
                goto exit;
            }
            processor_index++;
            amxd_trans_set_value(cstring_t, trans, "Architecture", architecture);
            amxd_trans_select_pathf(trans, ".^");
        }

    }

    status = amxd_status_ok;

exit:
    if(p_file != NULL) {
        if(fclose(p_file) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to close the /proc/cpuinfo");
        }
    }
    return status;

}


amxd_status_t get_processors(amxd_dm_t* dm) {

    amxd_status_t status = amxd_status_unknown_error;
    struct utsname uname_info;

    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "%sDeviceInfo.Processor.", prefix_());

    if(uname(&uname_info) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to get information about architecture");
        goto exit;
    }

    status = add_instances(&trans, uname_info.machine);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to add instances");
        goto exit;
    }

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction");
        goto exit;
    }

    status = amxd_status_ok;
exit:
    amxd_trans_clean(&trans);
    return status;
}