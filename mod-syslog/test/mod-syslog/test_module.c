/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "mod_syslog.h"
#include "syslog_dm.h"
#include "syslog_events.h"
#include "test_module.h"
#include "../common/mock.h"

bool allow_logrotate = false;
bool allow_vendorlogfiles = false;

int test_setup(void** state) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;

    amxut_bus_setup(state);
    init_test_variants();

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    assert_int_equal(amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0), 0);

    if(0 != amxo_parser_parse_file(parser, "../odl/mock.odl", root)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&amxut_bus_parser()->msg, 0));
    }
    if(0 != amxo_parser_parse_file(parser, "../odl/mock_syslog.odl", root)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&amxut_bus_parser()->msg, 0));
    }
    if(0 != amxo_parser_parse_file(parser, "../../../odl/deviceinfo-manager_definition.odl", root)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&amxut_bus_parser()->msg, 0));
    }

    amxut_bus_handle_events();
    return 0;
}

int test_teardown(void** state) {
    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 1 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 1 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");

    cleanup_syslog_subscriptions();
    cleanup_test_variants();

    amxm_close_all();
    amxut_bus_handle_events();
    amxo_resolver_import_close_all();
    amxut_bus_handle_events();
    amxut_bus_teardown(state);

    return 0;
}

void test_startup(UNUSED void** state) {
    assert_nr_deviceinfo_vendorlogfiles(0);
    assert_nr_deviceinfo_logrotates(0);
    init_settings();
    populate_deviceinfo_vendorlogfiles();
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 1 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 1 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    init_syslog_subscriptions();
    amxut_bus_handle_events();
}

void test_add_syslog_entry_with_empty_filepath(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Syslog.Action");
    amxd_trans_add_inst(&trans, 0, "messages");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 1 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 1 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
}

void test_fix_syslog_entry_with_empty_filepath(UNUSED void** state) {
    amxc_var_t file_path;
    amxd_trans_t trans;

    amxc_var_init(&file_path);
    amxd_trans_init(&trans);

    amxc_var_set(cstring_t, &file_path, "file:///var/log/messages");

    amxd_trans_select_pathf(&trans, "Syslog.Action.messages");
    amxd_trans_set_param(&trans, "LogFile.FilePath", &file_path);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxc_var_clean(&file_path);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 2 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 2 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.2.", "file:///var/log/messages");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-messages", "file:///var/log/messages");
}

void test_add_syslog_entry_with_valid_filepath(UNUSED void** state) {
    amxc_var_t file_path;
    amxd_trans_t trans;

    amxc_var_init(&file_path);
    amxd_trans_init(&trans);

    amxc_var_set(cstring_t, &file_path, "file:///var/log/firewall");

    amxd_trans_select_pathf(&trans, "Syslog.Action");
    amxd_trans_add_inst(&trans, 0, "firewall");
    amxd_trans_set_param(&trans, "LogFile.FilePath", &file_path);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxc_var_clean(&file_path);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 3 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 3 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.2.", "file:///var/log/messages");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.3.", "file:///var/log/firewall");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-messages", "file:///var/log/messages");
    assert_deviceinfo_logrotate("syslog-firewall", "file:///var/log/firewall");
}

void test_change_syslog_entry_filepath(UNUSED void** state) {
    amxc_var_t file_path;
    amxd_trans_t trans;

    amxc_var_init(&file_path);
    amxd_trans_init(&trans);

    amxc_var_set(cstring_t, &file_path, "file:///data/log/firewall");

    amxd_trans_select_pathf(&trans, "Syslog.Action.firewall");
    amxd_trans_set_param(&trans, "LogFile.FilePath", &file_path);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxc_var_clean(&file_path);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 3 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 3 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.2.", "file:///var/log/messages");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.3.", "file:///data/log/firewall");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-messages", "file:///var/log/messages");
    assert_deviceinfo_logrotate("syslog-firewall", "file:///data/log/firewall");
}

void test_remove_syslog_entry(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Syslog.Action.");
    amxd_trans_del_inst(&trans, 0, "messages");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 2 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 2 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.3.", "file:///data/log/firewall");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-firewall", "file:///data/log/firewall");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Syslog.Action.");
    amxd_trans_del_inst(&trans, 0, "firewall");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
    assert_nr_deviceinfo_vendorlogfiles(allow_vendorlogfiles ? 1 : 0);
    assert_nr_deviceinfo_logrotates(allow_logrotate ? 1 : 0);
    assert_deviceinfo_vendorlogfile("DeviceInfo.VendorLogFile.1.", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
    assert_deviceinfo_logrotate("syslog-test_action", "file:///tmp/test_tr181-syslog/test_action.test_file_out.log");
}
