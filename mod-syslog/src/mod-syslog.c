/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslog.h"
#include "syslog_dm.h"
#include "syslog_events.h"

static bool sync_vendorlogfiles = true;
static bool sync_logrotate = true;

bool sync_vendorlogfiles_allowed(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return sync_vendorlogfiles;
}

bool sync_logrotate_allowed(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return sync_logrotate;
}

bool syslog_sync_needed(void) {
    bool result = false;
    SAH_TRACEZ_IN(ME);
    result = sync_vendorlogfiles_allowed() || sync_logrotate_allowed();
    SAH_TRACEZ_OUT(ME);
    return result;
}

int init_settings(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t params;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_var_init(&params);

    rv = amxm_execute_function("self", MOD_CORE, "get-syslog-config", &params, &ret);
    when_failed_trace(rv, exit, WARNING, "Mod %s, func 'get-syslog-config' returned %d", MOD_CORE, rv);

    sync_vendorlogfiles = GET_BOOL(&params, "sync_vendorlog");
    sync_logrotate = GET_BOOL(&params, "sync_logrotate");

exit:
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int mod_start_syslog_sync(UNUSED const char* function_name,
                          UNUSED amxc_var_t* params,
                          UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    wait_for_syslog();
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_CONSTRUCTOR mod_syslog_start(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    SAH_TRACEZ_INFO(ME, "Starting mod-syslog");

    amxm_module_register(&mod, so, MOD_CTRL);
    amxm_module_add_function(mod, "start-syslog-sync", mod_start_syslog_sync);

    rv = init_settings();
    when_failed_trace(rv, exit, ERROR, "Could not load syslog settings, using defaults!");

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static AMXM_DESTRUCTOR mod_syslog_stop(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_WARNING(ME, "Stopping mod-syslog");

    cleanup_syslog_subscriptions();

    SAH_TRACEZ_OUT(ME);
    return 0;
}
