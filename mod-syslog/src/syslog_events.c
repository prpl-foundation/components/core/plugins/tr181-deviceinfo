/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_types.h>
#include <amxp/amxp_slot.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "syslog_dm.h"
#include "syslog_events.h"
#include "syslog_helpers.h"
#include "mod_syslog.h"

static amxb_subscription_t* syslog_action_logfile_added = NULL;
static amxb_subscription_t* syslog_action_logfile_changed = NULL;
static amxb_subscription_t* syslog_action_logfile_removed = NULL;

static int commit(bool update_syslog_dm, amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* list_item = NULL;
    amxc_var_t updated_files;
    int rv = -1;

    amxc_var_init(&updated_files);

    when_null(params, exit);
    when_false_trace(syslog_sync_needed(), exit, ERROR, "This function should not be called since no sync is required!");

    amxc_var_set_type(&updated_files, AMXC_VAR_ID_LIST);
    list_item = amxc_var_add_new(&updated_files);
    amxc_var_set_type(list_item, AMXC_VAR_ID_HTABLE);
    amxc_var_move(list_item, params);

    rv = update_datamodels(&updated_files, update_syslog_dm);

exit:
    amxc_var_clean(&updated_files);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static char* get_alias_from_object_path(const char* path, const char* prefix) {
    char* result = NULL;
    amxc_string_t alias;

    amxc_string_init(&alias, 0);
    amxc_string_setf(&alias, "%s", path);
    amxc_string_replace(&alias, ".LogFile.", "", 1);
    amxc_string_replace(&alias, "Syslog.Action.", "", 1);
    amxc_string_prependf(&alias, "%s", prefix);
    result = amxc_string_take_buffer(&alias);

    amxc_string_clean(&alias);
    return result;
}

void syslog_action_logfile_added_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* changed_parameters = NULL;
    amxc_var_t params;
    char* syslog_action_alias = NULL;

    amxc_var_init(&params);

    when_null(data, exit);
    when_false_trace(syslog_sync_needed(), exit, ERROR, "This function should not be called since no sync is required!");

    changed_parameters = GET_ARG(data, "parameters");
    when_null(changed_parameters, exit);
    when_str_empty_trace(GET_CHAR(changed_parameters, "FilePath"), exit, WARNING, "Skipping %s since FilePath is empty", GET_CHAR(data, "path"));

    syslog_action_alias = get_alias_from_object_path(GET_CHAR(data, "object"), VENDORLOGFILE_SYSLOG_ALIAS_PREFIX);
    when_failed(populate_logfile_settings_htable("add", &params, GET_ARG(data, "parameters"), GET_CHAR(data, "path"), syslog_action_alias), exit);
    when_failed(commit(true, &params), exit);
exit:
    free(syslog_action_alias);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
}

void syslog_action_logfile_changed_cb(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t* changed_parameters = NULL;
    amxc_var_t* file_path = NULL;
    amxc_string_t vendor_logfile_ref_path;
    amxc_var_t ret;
    amxc_var_t params;
    bool update_syslog_dm = false;
    char* syslog_action_alias = NULL;

    amxc_var_init(&params);
    amxc_string_init(&vendor_logfile_ref_path, 0);
    amxc_var_init(&ret);

    when_null(data, exit);
    when_false_trace(syslog_sync_needed(), exit, ERROR, "This function should not be called since no sync is required!");

    changed_parameters = GET_ARG(data, "parameters");
    when_null(changed_parameters, exit);

    file_path = GET_ARG(changed_parameters, "FilePath");
    when_null(file_path, exit);

    syslog_action_alias = get_alias_from_object_path(GET_CHAR(data, "object"), VENDORLOGFILE_SYSLOG_ALIAS_PREFIX);

    amxc_string_setf(&vendor_logfile_ref_path, "%sVendorLogFileRef", GET_CHAR(data, "object"));

    rv = amxb_get(syslog_ctx(), amxc_string_get(&vendor_logfile_ref_path, 0), UINT32_MAX, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Could not get VendorLogFileRef for changed Syslog object");

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "SyslogActionLogFilePath", GET_CHAR(file_path, "to"));
    amxc_var_add_key(cstring_t, &params, "SyslogActionAlias", syslog_action_alias);
    if(STR_EMPTY(GET_CHAR(file_path, "from"))) { // Since the Syslog.Action.*.LogFile.FilePath was empty before, a DeviceInfo.VendorLogFile was not created yet
        amxc_var_add_key(cstring_t, &params, "Action", "add");
        amxc_var_add_key(cstring_t, &params, "SyslogVendorLogFileRef", "");
        amxc_var_add_key(cstring_t, &params, "SyslogLogFileRef", GET_CHAR(data, "path"));
        amxc_var_add_key(uint32_t, &params, "MaximumSize", 0);
        amxc_var_add_key(bool, &params, "Persistent", false);
        update_syslog_dm = true;
    } else {
        amxc_var_add_key(cstring_t, &params, "Action", "update");
        amxc_var_add_key(cstring_t, &params, "SyslogVendorLogFileRef", GETP_CHAR(&ret, "0.0.VendorLogFileRef"));
    }

    when_failed(commit(update_syslog_dm, &params), exit);

    SAH_TRACEZ_INFO(ME, "%s Changed: FilePath %s -> %s", GETP_CHAR(&ret, "0.0.VendorLogFileRef"), GET_CHAR(file_path, "from"), GET_CHAR(file_path, "to"));

exit:
    free(syslog_action_alias);
    amxc_var_clean(&params);
    amxc_var_clean(&ret);
    amxc_string_clean(&vendor_logfile_ref_path);
    SAH_TRACEZ_OUT(ME);
}

void syslog_action_logfile_removed_cb(UNUSED const char* const sig_name,
                                      const amxc_var_t* const data,
                                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t params;
    char* syslog_action_alias = NULL;
    amxc_var_init(&params);

    when_null(data, exit);
    when_false_trace(syslog_sync_needed(), exit, ERROR, "This function should not be called since no sync is required!");

    syslog_action_alias = get_alias_from_object_path(GET_CHAR(data, "object"), VENDORLOGFILE_SYSLOG_ALIAS_PREFIX);
    when_failed(populate_logfile_settings_htable("remove", &params, GET_ARG(data, "parameters"), GET_CHAR(data, "path"), syslog_action_alias), exit);
    when_failed(commit(false, &params), exit);

exit:
    free(syslog_action_alias);
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
}

void init_syslog_subscriptions(void) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = NULL;
    int ret = -1;

    when_false_status(syslog_sync_needed(), exit, ret = 0);

    ctx = syslog_ctx();
    when_null_trace(ctx, exit, ERROR, "Could not find Syslog bus context");

    ret = amxb_subscription_new(&syslog_action_logfile_added,
                                ctx,
                                SYSLOG_PATH,
                                "(notification in ['dm:object-added']) and \
                                 (path matches 'Syslog\\.Action\\.[0-9]+\\.LogFile\\.$')",
                                syslog_action_logfile_added_cb,
                                NULL);
    when_failed_trace(ret, exit, ERROR, "Could not create syslog logfile added subscription");

    ret = amxb_subscription_new(&syslog_action_logfile_changed,
                                ctx,
                                SYSLOG_PATH,
                                "(notification in ['dm:object-changed']) and \
                                 (path matches 'Syslog\\.Action\\.[0-9]+\\.LogFile\\.$') and \
                                  contains('parameters.FilePath')",
                                syslog_action_logfile_changed_cb,
                                NULL);
    when_failed_trace(ret, exit, ERROR, "Could not create syslog logfile changed subscription");

    ret = amxb_subscription_new(&syslog_action_logfile_removed,
                                ctx,
                                SYSLOG_PATH,
                                "(notification in ['dm:object-removed']) and \
                                 (path matches 'Syslog\\.Action\\.[0-9]+\\.LogFile\\.$')",
                                syslog_action_logfile_removed_cb,
                                NULL);
    when_failed_trace(ret, exit, ERROR, "Could not create syslog logfile removed subscription");

exit:
    SAH_TRACEZ_OUT(ME);
}

void cleanup_syslog_subscriptions(void) {
    SAH_TRACEZ_IN(ME);

    amxb_subscription_delete(&syslog_action_logfile_added);
    amxb_subscription_delete(&syslog_action_logfile_changed);
    amxb_subscription_delete(&syslog_action_logfile_removed);

    SAH_TRACEZ_OUT(ME);
}
