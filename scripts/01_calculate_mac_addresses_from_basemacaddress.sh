#!/bin/sh

source /lib/functions/system.sh
source /var/etc/environment
if [ -f "/etc/prplconfig" ]; then
    source /etc/prplconfig
fi

if grep -q "CLONEMACADDRESS=" "/var/etc/environment"; then
    BASEMACADDRESS=$CLONEMACADDRESS
else
    BASEMACADDRESS=$HWMACADDRESS
fi

echo "export BASEMACADDRESS=\"$BASEMACADDRESS\"" >> /var/etc/environment

if [ -z "$CONFIG_SAH_AMX_TR181_DEVICEINFO_NUMBER_OF_EXTRA_MAC_ADDRESSES" ]; then
    CONFIG_SAH_AMX_TR181_DEVICEINFO_NUMBER_OF_EXTRA_MAC_ADDRESSES=7
fi

for i in $(seq 1 "$CONFIG_SAH_AMX_TR181_DEVICEINFO_NUMBER_OF_EXTRA_MAC_ADDRESSES"); do
    if [ -z $BASEMACADDRESS ]; then
        mac=""
    else
        mac=$(macaddr_add $BASEMACADDRESS $i)
        mac=$(echo "$mac" | tr '[a-z]' '[A-Z]')
    fi 
    echo "export BASEMACADDRESS_PLUS_$i=\"$mac\"" >> /var/etc/environment
done
