#!/bin/sh
#Script installed by tr181-deviceinfo.

NEW_DIRECTORY="/data/log"
COMPRESSION_EXTENSION=".gz"
LOGROTATE_ADD_SCRIPT="/usr/lib/debuginfo/logrotate_add.sh"
LOGROTATE_REMOVE_SCRIPT="/usr/lib/debuginfo/logrotate_remove.sh"
LOGSIZE=10000000 # 10Mb
CREATE_TAR_GZ=0
MAXIMUM_TARBALLS=5
