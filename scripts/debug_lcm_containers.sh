#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

print_syslog_file()
{
    # assumption: if we have gz log-files on containers, we assume zcat is also there
    case "$1" in
    *.gz )
        zcat $1 2>/dev/null
        ;;
    *)
        cat $1  2>/dev/null
        ;;
    esac
}

dump_containers_log()
{
    FILES=`ls -1rt /var/log/lcm/*/messages 2>/dev/null` 

    if [ -n "${FILES}" ]; then
        show_title "Show all containers logs"
        for i in $FILES; do
            show_title " ##|$i|##"
            print_syslog_file $i
        done
    fi
}

dump_containers_log
