#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="deviceinfo-manager"
datamodel_root="DeviceInfo"

prepare(){
    hash=$(software_version_hash.sh)
    hash_old=$(cat /cfg/dbversion)
    if [ "$hash" != "$hash_old" ]; then
        export LAST_UPGRADE_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
    fi
}

case $1 in
    boot)
        prepare
        process_boot ${name} -D
        ;;
    start)
        prepare
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
