#!/bin/sh

####################################################################
#How to use: tmpfs_download_mount.sh PROC_OWNER FILE_SIZE
#
#PROC_OWNER - The name of the plugin or process that is executing the script, the partition
#             manipulation will be locked to this specific owner.
#FILE_SIZE - The size of the file that is going to be added after mounting the partition.
#            If the partition is mounted, and the allocation is not enough the partition will grow in order to accomodate the file.
#
# After an amount of seconds specified in DeviceInfo.DownloadPathDuration, the partition will be destroyed.
#
#ATTENTION: Any change to this script might break other plugins that use it!
####################################################################

lockfile="/tmp/tmpfs_download_script.lock"
while [ -e "$lockfile" ]; do USELESS_VAR=""; done
touch "$lockfile"
trap 'rm -f "$lockfile"' EXIT

PROC_OWNER="$1" #Name of the plugin or process executing the script
FILE_SIZE=$2 #File size in Mb
export PARAM_PARTITION_PATH="$3"
export PARAM_PARTITION_SIZE="$4"
export PARAM_PARTITION_DURATION="$5"

. /usr/lib/amx/scripts/tmpfs_download_var.sh $3 $4 $5

[ -z "$PROC_OWNER" ] && exit 1
[ -z "$FILE_SIZE" ] && exit 1
! expr "$FILE_SIZE" : '[0-9]\+$' > /dev/null && exit 1 #Check if FILE_SIZE is an integer
FILE_SIZE=`expr $FILE_SIZE + 2` #1MB for possible rounding issues and 1MB for metadata

#Validate variables from /usr/lib/amx/scripts/tmpfs_download_var.sh
[ -z "$PARTITION_TIME_FILE" ] && exit 1
[ -z "$PARTITION_OWNER_FILE" ] && exit 1
[ -z "$PARTITION_DURATION_FILE" ] && exit 1
[ -z "$PARTITION_PATH_FILE" ] && exit 1
[ -z "$PARTITION_CRONTAB_FILE" ] && exit 1
[ -z "$PARTITION_GLOBAL_CRONTAB_FILE" ] && exit 1
[ -z "$PARTITION_UNMOUNT_SCRIPT" ] && exit 1
[ ! -f "$PARTITION_UNMOUNT_SCRIPT" ] && exit 1

if [ -z "$PARTITION_SIZE" ]; then
    PARTITION_SIZE=$PARTITION_DEFAULT_SIZE
    if [ -z "$PARTITION_SIZE" ]; then
        PARTITION_SIZE=0
    fi
fi
! expr "$PARTITION_SIZE" : '[0-9]\+$' > /dev/null && exit 1 #Check if PARTITION_SIZE is an integer

if [ -z "$PARTITION_PATH" ]; then
    PARTITION_PATH=$PARTITION_DEFAULT_PATH
fi
[ -z "$PARTITION_PATH" ] && exit 1

if [ -z "$PARTITION_DURATION" ]; then
    PARTITION_DURATION=$PARTITION_DEFAULT_DURATION
fi
! expr "$PARTITION_DURATION" : '[0-9]\+$' > /dev/null && exit 1 #Check if PARTITION_DURATION is an integer

#Avoid doing stuff on unwanted directories
case "$PARTITION_PATH" in
    "/" | \
    *"/bin"* | \
    *"/bootfs"* | \
    *"/cfg"* | \
    *"/data"* | \
    *"/dev"* | \
    *"/etc"* | \
    *"/home"* | \
    *"/include"* | \
    *"/lcm"* | \
    *"/lib"* | \
    *"/lib64"* | \
    *"/mnt"* | \
    *"/overlay"* | \
    *"/proc"* | \
    *"/rom"* | \
    *"/root"* | \
    *"/sbin"* | \
    *"/sys"* | \
    *"/tmp"* | \
    *"/usr"* | \
    *"/webui"* | \
    *"/www"*)
        exit
        ;;
esac

if [ -d "$PARTITION_PATH" ]; then
    #Check if the partition exists
    PATH_WITHOUT_SLASH="${PARTITION_PATH%/}"
    PARTITION_IS_CREATED=`df -h "$PATH_WITHOUT_SLASH" 2> /dev/null | grep tmpfs 2> /dev/null || true`
    if [ -n "$PARTITION_IS_CREATED" ]; then
        #The PARTITION_OWNER_FILE is a file that contains the name of the process or plugin that mounted the partition. 
        CURRENT_OWNER=`head -n 1 $PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_OWNER_FILE | cut -d' ' -f1 2> /dev/null || true`
        if [ -n "$CURRENT_OWNER" ]; then
            [ "$PROC_OWNER" != "$CURRENT_OWNER" ] && exit 1
        fi

        DF_CMD=`df -m $PARTITION_PATH 2> /dev/null || true`
        [ -z "$DF_CMD" ] && exit 1

        REMAINING_SIZE=`echo $DF_CMD | cut -d' ' -f11 2> /dev/null || true`
        [ -z "$REMAINING_SIZE" ] && exit 1
        ! expr "$REMAINING_SIZE" : '[0-9]\+$' > /dev/null && exit 1 #Check if REMAINING_SIZE is an integer

        USED_SIZE=`echo $DF_CMD | cut -d' ' -f10 2> /dev/null || true`
        [ -z "$USED_SIZE" ] && exit 1
        ! expr "$USED_SIZE" : '[0-9]\+$' > /dev/null && exit 1 #Check if USED_SIZE is an integer
        USED_SIZE=`expr $USED_SIZE + 1` #1MB for possible rounding issues

        if [ $FILE_SIZE -gt $REMAINING_SIZE ]; then
            TMPFS_SIZE=`expr $USED_SIZE + $FILE_SIZE`
            echo Expanding tmpfs partition at $PARTITION_PATH.
            mount -o remount,size=${TMPFS_SIZE}M $PARTITION_PATH && exit 1
        fi
    else
        DIRECTORY_IS_EMPTY=`ls $PARTITION_PATH 2> /dev/null || true` #Check if the directory is a valid directory which should not contain any file or directory inside it.
        if [ -z "$DIRECTORY_IS_EMPTY" ]; then
            PERMISSION_NUMBER=`ls -ld $PARTITION_PATH | awk '{k=0;for(i=0;i<=8;i++)k+=((substr($1,i+2,1)~/[rwx]/)*2^(8-i));printf("%0o\n",k)}' 2> /dev/null || true`
            if [ "$PERMISSION_NUMBER" == "776" ]; then
                rm -rf $PARTITION_PATH
            fi
        fi
        if [ $FILE_SIZE -gt $PARTITION_SIZE ]; then
            TMPFS_SIZE=`expr $FILE_SIZE + 1` #add 1MB for metadata
        else
            TMPFS_SIZE=$PARTITION_SIZE
        fi
        echo "Mounting tmpfs partition at $PARTITION_PATH."
        mkdir -p $PARTITION_PATH
        mount -t tmpfs -o size=${TMPFS_SIZE}M tmpfs $PARTITION_PATH
    fi
else
    if [ $FILE_SIZE -gt $PARTITION_SIZE ]; then
        TMPFS_SIZE=`expr $FILE_SIZE + 1` #add 1MB for metadata
    else
        TMPFS_SIZE=$PARTITION_SIZE
    fi

    echo "Mounting tmpfs partition at $PARTITION_PATH."
    mkdir -p $PARTITION_PATH
    mount -t tmpfs -o size=${TMPFS_SIZE}M tmpfs $PARTITION_PATH
fi

#Mount the metadata partition and store the metadata
if [ ! -d "$PARTITION_PATH/$PARTITION_METADATA_FOLDER" ]; then
    mkdir -p "$PARTITION_PATH/$PARTITION_METADATA_FOLDER"
    mount -t tmpfs -o size=50K tmpfs $PARTITION_PATH/$PARTITION_METADATA_FOLDER/
else
    mount -o remount,rw $PARTITION_PATH/$PARTITION_METADATA_FOLDER
fi

TIMESTAMP=`date +%@`
echo $TIMESTAMP > $PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_TIME_FILE
echo $PARTITION_DURATION > $PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_DURATION_FILE
[ ! -f "$PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_OWNER_FILE" ] && echo "$PROC_OWNER" > $PARTITION_PATH/$PARTITION_METADATA_FOLDER/$PARTITION_OWNER_FILE
mount -o remount,ro $PARTITION_PATH/$PARTITION_METADATA_FOLDER/

#Mount the safe partition to store the partition path
if [ ! -d "$PARTITION_SAFE_TMPFS_PARTITION" ]; then
    mkdir -p "$PARTITION_SAFE_TMPFS_PARTITION"
    mount -t tmpfs -o size=10K tmpfs $PARTITION_SAFE_TMPFS_PARTITION
else
    mount -o remount,rw $PARTITION_SAFE_TMPFS_PARTITION
fi
echo $PARTITION_PATH > $PARTITION_PATH_FILE
mount -o remount,ro $PARTITION_SAFE_TMPFS_PARTITION

if [ $PARTITION_DURATION -ne 0 ]; then
    #Convert seconds to minutes
    PARTITION_DURATION_MIN=`expr $PARTITION_DURATION / 60 2> /dev/null || echo 5`
    PARTITION_DURATION_REMAINDER=`expr $PARTITION_DURATION % 60 2> /dev/null || echo 0`
    if [ $PARTITION_DURATION_REMAINDER -gt 0 ]; then
        PARTITION_DURATION_MIN=`expr $PARTITION_DURATION_MIN + 1 2> /dev/null || echo 5`
    fi
fi

#Create the crontab job to delete the partition after PARTITION_DURATION seconds
tmpfs_delete_crontab_job || true

#Apply the crontab job
if [ $PARTITION_DURATION -ne 0 ]; then
    [ -f "$PARTITION_CRONTAB_FILE" ] && rm -rf $PARTITION_CRONTAB_FILE
    echo "#############################  START of tmpfs generated crontabs #############################" >> $PARTITION_CRONTAB_FILE
    echo "# tr181-deviceinfo download tmpfs generated crontabs." >> $PARTITION_CRONTAB_FILE
    echo "#    do not change any values in this tmpfs crontab block."  >> $PARTITION_CRONTAB_FILE
    echo "*/$PARTITION_DURATION_MIN * * * * /bin/sh $PARTITION_UNMOUNT_SCRIPT root" >> $PARTITION_CRONTAB_FILE
    echo "##############################  END of tmpfs generated crontabs ##############################" >> $PARTITION_CRONTAB_FILE

    tmpfs_create_crontab_job
fi
