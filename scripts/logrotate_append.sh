#!/bin/sh
#Script installed by tr181-deviceinfo.

# This script can be used to append logs to a different file (on persistent storage).
# Make sure to add this script path to the tr181-deviceinfo ODL config variable "logrotate-postrotate-script".
# IMPORTANT: This script calls the LOGROTATE_ADD_SCRIPT to make the rotated logfiles available in the DM.
#            By default it is already called by the plugin but we want to disable it there to avoid double entries in the DM.
#            To do this you have to disable config variable "logrotate-enable-add-script" in the ODL config.

# Load constants from logrotate_var.sh
[ -f /usr/lib/debuginfo/logrotate_var.sh ] && . /usr/lib/debuginfo/logrotate_var.sh

#Check for at least 2 arguments (1: rotated file, 2: DM alias)
if [ $# -lt 2 ]; then
    exit 22
fi

ROTATED_FILE="$1"
ALIAS="$2"

remove_logfile_from_dm(){
    logfile_to_remove="$1"

    # Remove logfile from DM
    json="{action=remove,alias=$ALIAS,filename=$logfile_to_remove}"
    ba-cli -a "DeviceInfo.UpdateLogRotate(mod_namespace=lr-ctrl,mod_name=mod-lr-logrotate,fnc=handle-script,data=[$json])"
}

# ROTATED_FILE contains the absolute path of the file that is rotated
OLD_DIRECTORY="$(dirname "$ROTATED_FILE")" # e.g: /var/log
FILE="$(basename "$ROTATED_FILE")"         # e.g: messages

# Append logs rotated by logrotate to a new file
ROTATED_LOGS=$(find "$OLD_DIRECTORY" -type f -name "$FILE.*")
for log in $ROTATED_LOGS; do
    cat "$log" >> "$NEW_DIRECTORY"/"$FILE"
    rm "$log"
done

test ! -f "$NEW_DIRECTORY/$FILE" && \
    echo "$NEW_DIRECTORY/$FILE not found" && \
    exit

# Create tarball if needed
if test "$CREATE_TAR_GZ" -eq 1 && [ "$(stat -c %s "$NEW_DIRECTORY"/"$FILE")" -gt "$LOGSIZE" ]; then
    # Create tarball and remove logfile
    NB_LOGENTRY="$(ba-cli -l "DeviceInfo.LogRotate.${ALIAS}.LogFileNumberOfEntries?" | tr -d '\n')"
    test -z "$NB_LOGENTRY" && exit
    rotated_logs="$NB_LOGENTRY"

    test "$NB_LOGENTRY" -ge "$MAXIMUM_TARBALLS" && \
        rotated_logs="$((MAXIMUM_TARBALLS-1))"

    for i in $(seq "$rotated_logs" -1 1); do
        old_name="$NEW_DIRECTORY/${FILE}.${i}.tar.gz"
        new_name="$NEW_DIRECTORY/${FILE}.$((i+1)).tar.gz"
        mv "$old_name" "$new_name"
    done

    tar zcf "$NEW_DIRECTORY/${FILE}.1.tar.gz" -C "$NEW_DIRECTORY" "$FILE" && rm "$NEW_DIRECTORY/$FILE"
fi

# add all log file that start with $FILE in $NEW_DIRECTORY to logrotate
"$LOGROTATE_ADD_SCRIPT" "$NEW_DIRECTORY/$FILE" "$ALIAS"
