#!/bin/sh
#Script installed by tr181-deviceinfo.
JSON_LIST=""

#Check for at least 2 arguments (1: rotated file, 2: DM alias)
if [ $# -lt 2 ]; then
    exit 22
fi

FILENAME_PATH=$1  # e.g: /var/log/messages
ALIAS=$2          # e.g: cpe-LogRotate-1

DIRECTORY=$(dirname "$FILENAME_PATH") # e.g: /var/log

if [ ! -d "$DIRECTORY" ]; then
    exit 2
fi

JSON_LIST="[{action=add,alias="$ALIAS",directory="$DIRECTORY"}]"
ba-cli -a "DeviceInfo.UpdateLogRotate(mod_namespace=lr-ctrl,mod_name=mod-lr-logrotate,fnc=handle-script,data=$JSON_LIST)"
