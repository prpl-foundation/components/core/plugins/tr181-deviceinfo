#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

show_title "show meminfos"
show_cmd cat /proc/meminfo

show_title "show slabinfo"
show_cmd $SUDO cat /proc/slabinfo

show_title "show disk usage"
show_cmd $SUDO du / -d 1 -h

show_title "show df information"
show_cmd df
