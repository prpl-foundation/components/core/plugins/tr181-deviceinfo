cleanup() {
    rm -rf /cfg/etc/*
    rm -rf /cfg/system/*
    rm -rf /cfg/prplos-config/*
}

do_trigger_restore() {

    dbversion_file="/cfg/dbversion"
    directory="/cfg/pcm/"
    upgrade_occurred_file="/cfg/upgrade_occurred"

    if [ -f "$upgrade_occurred_file" ]; then
        cleanup && rm "$upgrade_occurred_file"
        echo "Reset, upgrade_occurred exists" > /dev/console
    elif [ -f "$dbversion_file" ]; then
        # Getting the old hash
        hash=$(cat /cfg/dbversion)

        # Creation of the new hash
        new_hash=$(software_version_hash.sh)

        # If the hash does not match, empty the following directories
        if [ "$hash" != "$new_hash" ]; then
            # Printing an error message to the console if /cfg/pcm/ is empty
            if [ -d "$directory" ] && [ -z "$(ls -A "$directory")" ]; then
                echo "Error: Directory '$directory' is empty and a reset has to be performed" > /dev/console
            fi
            cleanup
            echo "Reset, dbversion does not match or upgrade_occurred exists" > /dev/console
        else
            echo "No Reset, dbversion matches" > /dev/console
        fi    
    else
        # Printing an error message to the console if /cfg/pcm/ is empty
        if [ -d "$directory" ] && [ -z "$(ls -A "$directory")" ]; then
            echo "Error: Directory '$directory' is empty and a reset has to be performed" > /dev/console
        fi
        cleanup
        echo "Reset, dbversion and upgrade_occured do not exist" > /dev/console
    fi

}

boot_hook_add preinit_main do_trigger_restore
