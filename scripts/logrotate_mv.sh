#!/bin/sh
#Script installed by tr181-deviceinfo.

# This script can be used to move compressed logfiles from one directory to another.
# Make sure to add this script path to the tr181-deviceinfo ODL config variable "logrotate-lastaction-script".
# IMPORTANT: This script calls the LOGROTATE_ADD_SCRIPT to make the rotated logfiles available in the DM.
#            By default it is already called by the plugin but we want to disable it there to avoid double entries in the DM.
#            To do this you have to disable config variable "logrotate-enable-add-script" in the ODL config.

# Load constants from logrotate_var.sh
[ -f /usr/lib/debuginfo/logrotate_var.sh ] && source /usr/lib/debuginfo/logrotate_var.sh

#Check for at least 2 arguments (1: rotated file, 2: DM alias)
if [ $# -lt 2 ]; then
    exit 22
fi

# $1 contains the absolute path of the file that is rotated
OLD_DIRECTORY=$(dirname $1) # e.g: /var/log
FILE=$(basename $1)         # e.g: messages
if [[ $CREATE_TAR_GZ -eq 1 ]]
then
    EXTENSION=".tar.gz"
else
    EXTENSION="$COMPRESSION_EXTENSION"
fi

# The symlink that was created before can point to the wrong file since the files are rotated by logrotate.
# For example:
# -rw-r--r--    1 root     root         13428 Mar 19 11:05 messages.1
# -rw-r--r--    1 root     root          1429 Mar 19 11:05 messages.2.gz
# lrwxrwxrwx    1 root     root            19 Mar 19 11:05 messages.3.gz -> /data/log/messages.2.gz
# We have to follow the symlinks and rename the source to the correct name.
# Also remove the invalid symlinks as they are recreated later on in the script.
OLD_SYMLINKS=$(find "$OLD_DIRECTORY" -type l -name "$FILE.*$COMPRESSION_EXTENSION" | sort -r)
for L in $OLD_SYMLINKS; do
    FILE_NAME=$(basename $L)
    SOURCE=$(readlink -f $L)
    SOURCE_DIR=$(dirname $SOURCE)
    if [[ $CREATE_TAR_GZ -eq 1 ]]
    then
        mv "$SOURCE" "$SOURCE_DIR"/"$FILE_NAME""$EXTENSION"
    else
        mv "$SOURCE" "$SOURCE_DIR"/"$FILE_NAME"
    fi
    rm "$L" # Remove the symlink
done

# Move newly created files (not the symlinks) to NEW_DIRECTORY
# In case CREATE_TAR_GZ is equal to 1, logrotate should not compress the files ($COMPRESSION_EXTENSION should be empty).
# Otherwise the newly created files are compressed already and should just be moved to the new directory.
NEWLY_CREATED_FILES=$(find "$OLD_DIRECTORY" -type f -name "$FILE.*$COMPRESSION_EXTENSION")
for F in $NEWLY_CREATED_FILES; do
    if [[ $CREATE_TAR_GZ -eq 1 ]]
    then
        tar -czf "$NEW_DIRECTORY"/"$(basename $F)""$EXTENSION" "$F" #e.g: /var/log/messages.1 -> /data/logs/messages.1.tar.gz
        rm "$F"
    else
        mv "$F" "$NEW_DIRECTORY"
    fi
done

# Create symlinks for all moved files so logrotate knows how many files are rotated
MOVED_FILES=$(find "$NEW_DIRECTORY" -type f -name "$FILE.*$EXTENSION")
for F in $MOVED_FILES; do
    if [[ $CREATE_TAR_GZ -eq 1 ]]
    then
        ln -s "$F" "$OLD_DIRECTORY"/"$(basename $F | sed "s/$EXTENSION//")"
    else
        ln -s "$F" "$OLD_DIRECTORY"/
    fi
done

# Run once for moved files to new directory
"$LOGROTATE_ADD_SCRIPT" "$NEW_DIRECTORY"/"$FILE" "$2"

# !!!!!!!!!! IMPORTANT: Only do this if logrotate-enable-add-script is set to false in the tr181-deviceinfo ODL config settings.
# Run once for files that are still in old directory (files that are not compressed yet)
"$LOGROTATE_ADD_SCRIPT" "$1" "$2"
