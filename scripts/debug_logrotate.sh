#!/bin/sh
set -e

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh
LOGROTVAR=/usr/lib/debuginfo/logrotate_var.sh

test ! -f "$LIBDEBUG" && \
    echo "No debug library found : $LIBDEBUG" && \
    exit

test ! -f "$LOGROTVAR" && \
    echo "Logrotate variable definition not found : $LOGROTVAR" && \
    exit

. $LIBDEBUG
. $LOGROTVAR

DEST_DIR="$1"
LOG_DIR="$(source "$LOGROTVAR"; echo "$NEW_DIRECTORY")"
test -z "$DEST_DIR" && echo "error: no destination specified" && exit
test ! -d "$DEST_DIR" && echo "error: $DEST_DIR does not exists" && exit
test ! -d "$LOG_DIR" && echo "error: $LOG_DIR does not exists" && exit

cp "$LOG_DIR"/* "$DEST_DIR"
