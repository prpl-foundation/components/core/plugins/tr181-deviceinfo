/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "cpu_info.h"
#include "dm_process_info.h"
#include "deviceinfo.h"
#include "deviceinfo_firmwareImage.h"
#include "test_firmwareimage.h"
#include "common_mock.h"

int test_firmwareimage_setup(void** state) {
    return mock_setup(state, NULL);
}

int test_firmwareimage_teardown(void** state) {
    return mock_teardown(state);
}

void test_active_download_activate(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.FirmwareImage.1.");
    amxc_var_t args;
    int status = 0;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test.com/test.txt");
    amxc_var_add_key(bool, &args, "AutoActivate", true);

    status = _Download(object, NULL, &args, NULL);

    printf("status is %d\n", status);

    amxc_var_clean(&args);
}

void test_active_download_dont_activate(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.FirmwareImage.active.");
    amxc_var_t args;
    int status = 0;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test.com/test.txt");
    amxc_var_add_key(bool, &args, "AutoActivate", false);

    status = _Download(object, NULL, &args, NULL);

    printf("status is %d\n", status);

    amxc_var_clean(&args);
}

void test_inactive_download_activate(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.FirmwareImage.inactive.");
    amxc_var_t ret;
    amxc_var_t args;
    int status = 0;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test.com/test.txt");
    amxc_var_add_key(bool, &args, "AutoActivate", true);

    status = _Download(object, NULL, &args, &ret);

    printf("status is %d\n", status);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_inactive_download_dont_activate(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo.FirmwareImage.inactive.");
    amxc_var_t args;
    int status = 0;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "URL", "test.com/test.txt");
    amxc_var_add_key(bool, &args, "AutoActivate", false);

    status = _Download(object, NULL, &args, NULL);

    printf("status is %d\n", status);

    amxc_var_clean(&args);
}

void test_last_upgrade_date(UNUSED void** state) {
    amxc_ts_t tsp1 = {0};
    amxc_ts_t before = {0};
    amxc_ts_t after = {0};
    amxd_object_t* root = amxd_dm_findf(amxut_bus_dm(), "DeviceInfo");
    amxc_var_t value;

    amxc_var_init(&value);

    assert_non_null(root);

    amxd_object_get_param(root, "LastUpgradeDate", &value);
    assert_int_equal(amxc_ts_parse(&tsp1, "0001-01-01T00:00:00Z", strlen("0001-01-01T00:00:00Z")), 0);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &tsp1), 0);

    // first simulate firmware upgrade by setting env variable LAST_UPGRADE_DATE
    assert_int_equal(setenv("LAST_UPGRADE_DATE", "2024-01-01T00:00:00Z", 1), 0);
    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();
    amxd_object_get_param(root, "LastUpgradeDate", &value);
    assert_int_equal(amxc_ts_parse(&tsp1, "2024-01-01T00:00:00Z", strlen("2024-01-01T00:00:00Z")), 0);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &tsp1), 0);

    // simulate normal boot by unsetting env variable LAST_UPGRADE_DATE
    assert_int_equal(unsetenv("LAST_UPGRADE_DATE"), 0);
    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();
    amxd_object_get_param(root, "LastUpgradeDate", &value);
    assert_int_equal(amxc_ts_parse(&tsp1, "2024-01-01T00:00:00Z", strlen("2024-01-01T00:00:00Z")), 0);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &tsp1), 0);

    // simulate another firmware upgrade by setting env variable LAST_UPGRADE_DATE
    assert_int_equal(setenv("LAST_UPGRADE_DATE", "2024-01-02T23:59:59Z", 1), 0);
    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();
    amxd_object_get_param(root, "LastUpgradeDate", &value);
    assert_int_equal(amxc_ts_parse(&tsp1, "2024-01-02T23:59:59Z", strlen("2024-01-02T23:59:59Z")), 0);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &tsp1), 0);

    amxc_ts_now(&before);
    // simulate another firmware upgrade by setting env variable LAST_UPGRADE_DATE, but this time use garbage value, timestamp should be now
    assert_int_equal(setenv("LAST_UPGRADE_DATE", "garbage value", 1), 0);
    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();
    amxc_ts_now(&after);
    amxd_object_get_param(root, "LastUpgradeDate", &value);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &before), 1);
    assert_int_equal(amxc_ts_compare(amxc_var_constcast(amxc_ts_t, &value), &after), -1);

    amxc_var_clean(&value);
}