/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <dlfcn.h>

#include "common_mock.h"

#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "deviceinfo.h"
#include "deviceinfo_rpc.h"
#include "deviceinfo_dm_mngr.h"
#include "dm_process_info.h"
#include "deviceinfo_vendor_config_file.h"
#include "deviceinfo_vendor_log_file.h"
#include "deviceinfo_firmwareImage.h"
#include "deviceinfo_logrotate.h"

#define BACKUP_FILE "pcm.tar"
#define CREATE_PCM_USR_FILE "mkdir -p "BACKUP_DIR
#define CREATE_UPLOAD_DIR "mkdir -p "UPLOAD_DIR
#define CREATE_DOWNLOAD_DIR "mkdir -p "DOWNLOAD_DIR

static bool who_has_to_fail = false;
static bool disable_hostname_check = false;
static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;
static amxd_object_t* root = NULL;
static bool disable_check = true;

extern amxb_bus_ctx_t* __real_amxb_be_who_has(const char* object_path);

void set_who_has_to_fail(bool value) {
    who_has_to_fail = value;
}

void set_disable_hostname_check(bool value) {
    disable_hostname_check = value;
}

static int create_backup_file(const char* file_path, const char* content) {
    int ret = -1;
    FILE* file = NULL;

    file = fopen(file_path, "w");

    if(file != NULL) {
        fputs(content, file);
        fclose(file);
        ret = 0;
    }

    return ret;
}

amxd_status_t pcm_backup(UNUSED amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         UNUSED amxc_var_t* args,
                         amxc_var_t* ret) {
    const char* content = "This is a test backup file";
    amxc_string_t file_path;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "FileName", "testfile.tar");
    amxc_var_add_key(cstring_t, ret, "Alias", "testfile");

    amxc_string_init(&file_path, 0);
    amxc_string_setf(&file_path, "%s/%s", BACKUP_DIR, BACKUP_FILE);

    create_backup_file(amxc_string_get(&file_path, 0), content);
    amxc_string_clean(&file_path);

    return mock_type(int);
}

static amxd_status_t mock_backup(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    // because DeviceInfo and PCM both have rpc Backup
    if(strcmp("PersistentConfiguration", amxd_object_get_name(object, AMXD_OBJECT_NAMED)) == 0) {
        return pcm_backup(object, func, args, ret);
    }
    return _Backup(object, func, args, ret);
}

amxd_status_t pcm_restore(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    return mock_type(int);
}

static amxd_status_t mock_restore(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    // because DeviceInfo and PCM both have rpc Restore
    if(strcmp("PersistentConfiguration", amxd_object_get_name(object, AMXD_OBJECT_NAMED)) == 0) {
        return pcm_restore(object, func, args, ret);
    }
    return _Restore(object, func, args, ret);
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* object_path) {
    if(who_has_to_fail) {
        return NULL;
    }
    return __real_amxb_be_who_has(object_path);
}

// can't use linker to __wrap_ because this is a function called by libamxb
// mocked otherwise can't test wait for DNS. datamodel
amxb_bus_ctx_t* amxb_be_who_has_ex(const char* object_path, bool full_match) {
    void* handle = NULL;
    amxb_bus_ctx_t* bus = NULL;
    amxb_bus_ctx_t* (* real_amxb_be_who_has_ex)(const char* object_path, bool full_match);

    when_true(who_has_to_fail, exit);

    handle = dlopen("libamxb.so", RTLD_LAZY);
    dlerror();
    real_amxb_be_who_has_ex = dlsym(handle, __func__);
    bus = real_amxb_be_who_has_ex(object_path, full_match);
    dlclose(handle);

exit:
    return bus;
}

amxd_status_t pcm_AddBackupFile(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "FileName", "testfile.tar");
    amxc_var_add_key(cstring_t, ret, "Alias", "testfile");

    const char* content = "This is a test backup file";
    amxc_string_t file_path;

    amxc_string_init(&file_path, 0);
    amxc_string_setf(&file_path, "%s/%s", BACKUP_DIR, BACKUP_FILE);

    create_backup_file(amxc_string_get(&file_path, 0), content);
    amxc_string_clean(&file_path);
    return mock_type(int);
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so, UNUSED const char* shared_object_name, UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_close_all(void) {
    // calling amxm_close_all closes "self" which is bad for unit tests
    amxc_llist_for_each(it, amxm_get_so_list()) {
        amxm_shared_object_t* so = amxc_llist_it_get_data(it, amxm_shared_object_t, it);
        if(strcmp(so->name, "self") == 0) {
            amxm_module_t* mod = amxm_so_get_module(NULL, MOD_DM_MNGR);
            amxm_module_deregister(&mod);
            continue;
        }
        amxm_so_close(&so);
    }
    return 0;
}

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {

    if(strcmp(shared_object_name, MOD_HOSTNAME_ALIAS) == 0) {
        return mock_update_hostname(module_name, args, ret);
    }
    return 0;
}

int mock_update_hostname(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    if(disable_check) {
        return 0;
    }

    check_expected(args);
    return mock_type(int);
}

void set_disable_check(bool value) {
    disable_check = value;
}

amxd_dm_t* mock_get_dm(void) {
    return dm;
}

amxo_parser_t* mock_get_parser(void) {
    return parser;
}

amxd_object_t* mock_get_root(void) {
    return root;
}

int mock_setup(UNUSED void** state, const char* default_odl) {
    amxc_var_t* fields = NULL;

    amxut_bus_setup(NULL);

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    amxp_sigmngr_add_signal(NULL, "wait:done"); // normally amxrt does this, needed so that plugin can wait for DNS. object

    assert_int_equal(0, system(CREATE_PCM_USR_FILE));
    assert_int_equal(0, system(CREATE_UPLOAD_DIR));
    assert_int_equal(0, system(CREATE_DOWNLOAD_DIR));

    fields = amxc_var_add_key(amxc_htable_t, &parser->config, "process_field_names", NULL);
    amxc_var_add_key(cstring_t, fields, "pid", "PID");
    amxc_var_add_key(cstring_t, fields, "command", "Command");
    amxc_var_add_key(cstring_t, fields, "size", "Size");
    amxc_var_add_key(cstring_t, fields, "priority", "Priority");
    amxc_var_add_key(cstring_t, fields, "cpu_time", "CPUTime");
    amxc_var_add_key(cstring_t, fields, "state", "State");

    assert_int_equal(amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0), 0);

    assert_int_equal(amxo_resolver_ftab_add(parser, "hostname_changed", AMXO_FUNC(_hostname_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "last_upgrade_changed", AMXO_FUNC(_last_upgrade_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_status_cleanup", AMXO_FUNC(_process_status_cleanup)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "read_usage", AMXO_FUNC(_read_usage)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "cleanup_usage", AMXO_FUNC(_cleanup_usage)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_read", AMXO_FUNC(_process_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_list", AMXO_FUNC(_process_list)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_describe", AMXO_FUNC(_process_describe)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "process_cleanup", AMXO_FUNC(_process_cleanup)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Restore", AMXO_FUNC(mock_restore)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Backup", AMXO_FUNC(mock_backup)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Upload", AMXO_FUNC(_Upload)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "AddBackupFile", AMXO_FUNC(pcm_AddBackupFile)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Activate", AMXO_FUNC(_Activate)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Download", AMXO_FUNC(_Download)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "firmware_image_added", AMXO_FUNC(_firmware_image_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "call_ctrl", AMXO_FUNC(_call_ctrl)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "controllers_reload", AMXO_FUNC(_controllers_reload)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "log_rotate_added", AMXO_FUNC(_log_rotate_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "log_rotate_interval_changed", AMXO_FUNC(_log_rotate_interval_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "log_rotate_enable_changed", AMXO_FUNC(_log_rotate_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "log_rotate_changed", AMXO_FUNC(_log_rotate_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "log_rotate_removed", AMXO_FUNC(_log_rotate_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "UpdateLogRotate", AMXO_FUNC(_UpdateLogRotate)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "WriteUsersetting", AMXO_FUNC(_WriteUsersetting)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ReadUsersetting", AMXO_FUNC(_ReadUsersetting)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "uptime_onread", AMXO_FUNC(_UpTime_uptime_onread)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "total_onread", AMXO_FUNC(_Total_total_onread)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "free_onread", AMXO_FUNC(_Free_free_onread)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "SetVendorLogFile", AMXO_FUNC(_SetVendorLogFile)), 0);

    assert_int_equal(amxo_parser_parse_file(parser, "../odl/mock.odl", root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, "../../odl/deviceinfo-manager_definition.odl", root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, "../odl/00_deviceinfo-manager_defaults.odl", root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, "../odl/deviceinfo-manager_controllers.odl", root), 0);
    if(default_odl != NULL) {
        assert_int_equal(amxo_parser_parse_file(parser, default_odl, root), 0);
    }

    assert_int_equal(_deviceinfo_main(AMXO_START, dm, parser), 0);
    amxut_bus_handle_events();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();

    return 0;
}

int mock_teardown(UNUSED void** state) {
    assert_int_equal(_deviceinfo_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();
    amxo_resolver_import_close_all();
    amxut_bus_handle_events();
    amxut_bus_teardown(NULL);
    return 0;
}
