/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_hostname.h"

#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "deviceinfo.h"
#include "common_mock.h"

static int var_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* a = (amxc_var_t*) value;
    amxc_var_t* b = (amxc_var_t*) check_value_data;
    int result = -1;
    int rv = 0;

    print_message("<from plugin>\n");
    amxc_var_dump(a, 1);
    print_message("<from unit test>\n");
    amxc_var_dump(b, 1);

    if((amxc_var_compare(a, b, &result) != 0) ||
       (result != 0)) {
        goto exit;
    }

    rv = 1;

exit:
    amxc_var_delete(&b);
    return rv;
}

int test_setup(void** state) {
    int rv = -1;
    set_disable_check(true);
    rv = mock_setup(state, "mock_default.odl");
    set_disable_check(false);
    return rv;
}

int test_teardown(void** state) {
    return mock_teardown(state);
}

void test_change_hostname(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    assert_non_null(dm);

    amxut_dm_param_set(cstring_t, "DeviceInfo", "HostName", "new-name");
    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-1.", "Name", "new-name");
    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-2.", "Name", "new-name");
    amxut_dm_param_equals(cstring_t, "DNS.Host.host-1.", "Name", "hostname-1"); // should not have changed because Alias doesn't match regex
}

void test_wait_for_dns(UNUSED void** state) {
    amxd_dm_t* dm = amxut_bus_dm();
    assert_non_null(dm);

    set_who_has_to_fail(true);

    amxut_dm_param_set(cstring_t, "DeviceInfo", "HostName", "new-name");

    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-1.", "Name", "prplOS"); // not "default-name" as set in mock_default.odl because of sync during test setup (see deviceinfo defaults odl)
    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-2.", "Name", "prplOS"); // not "default-name" as set in mock_default.odl because of sync during test setup (see deviceinfo defaults odl)
    amxut_dm_param_equals(cstring_t, "DNS.Host.host-1.", "Name", "hostname-1");

    set_who_has_to_fail(false);

    amxut_dm_param_set(cstring_t, "DeviceInfo", "HostName", "other-name");

    amxp_sigmngr_emit_signal(NULL, "wait:done", NULL); // usually amxrt would do this when DNS plugin started
    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-1.", "Name", "other-name");
    amxut_dm_param_equals(cstring_t, "DNS.Host.deviceinfo-2.", "Name", "other-name");
    amxut_dm_param_equals(cstring_t, "DNS.Host.host-1.", "Name", "hostname-1");       // should not have changed because Alias doesn't match regex
}
