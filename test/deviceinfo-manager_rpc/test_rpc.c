/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "deviceinfo_rpc.h"
#include "deviceinfo.h"
#include "test_rpc.h"
#include "common_mock.h"

static bool create_dummy_file(const char* directory, const char* filename) {
    bool ret = false;
    amxc_string_t file_path;
    FILE* file = NULL;
    const char* content = "This is a test file";

    amxc_string_init(&file_path, 0);
    amxc_string_setf(&file_path, "%s/%s", directory, filename);

    file = fopen(amxc_string_get(&file_path, 0), "w");

    if(file != NULL) {
        fputs(content, file);
        fclose(file);
        ret = true;
    }

    amxc_string_clean(&file_path);
    return ret;
}

static void assert_params_value(const char* obj_path, amxc_var_t* expected) {
    amxd_object_t* target_obj = NULL;
    amxc_var_t dm_params;
    int compare_ret;

    amxc_var_init(&dm_params);

    target_obj = amxd_dm_findf(mock_get_dm(), obj_path);
    assert_non_null(target_obj);

    amxd_object_get_params(target_obj, &dm_params, amxd_dm_access_public);
    amxc_var_dump(&dm_params, STDOUT_FILENO);
    amxc_var_compare(&dm_params, expected, &compare_ret);
    assert_int_equal(compare_ret, 0);

    amxc_var_clean(&dm_params);
}

static void assert_nr_instances(const char* obj_path, uint32_t expected) {
    amxd_object_t* target_obj = NULL;
    uint32_t dm_inst_count = 0;
    uint32_t nr_ints = 1;

    target_obj = amxd_dm_findf(mock_get_dm(), obj_path);
    assert_non_null(target_obj);

    dm_inst_count = amxd_object_get_instance_count(target_obj);
    while(nr_ints < dm_inst_count + 1) {
        printf("Instance [%d] -> %s\n", nr_ints, amxd_object_get_name(amxd_object_get_instance(target_obj, NULL, nr_ints), AMXD_OBJECT_NAMED));
        nr_ints++;
    }

    assert_int_equal(dm_inst_count, expected);
}

int test_setup(void** state) {
    return mock_setup(state, NULL);
}

int test_teardown(void** state) {
    return mock_teardown(state);
}

void test_write_without_file(UNUSED void** state) {
    amxc_var_t args;
    const char* usersetting_status = NULL;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", "test");

    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "None");
    assert_int_equal(_WriteUsersetting(NULL, NULL, &args, NULL), amxd_status_unknown_error);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);
}

void test_cannot_write(UNUSED void** state) {
    amxc_var_t args;
    const char* filename = "test02";
    const char* usersetting_status = NULL;

    //Backup file doesn't exists
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);
    will_return(pcm_AddBackupFile, amxd_status_ok);
    assert_int_equal(_WriteUsersetting(NULL, NULL, &args, NULL), amxd_status_unknown_error);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);

    //Could not find bus for the pcm object
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);
    assert_true(create_dummy_file(UPLOAD_DIR, filename));
    set_who_has_to_fail(true);
    assert_int_equal(_WriteUsersetting(NULL, NULL, &args, NULL), amxd_status_unknown_error);
    set_who_has_to_fail(false);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);

    //Failure of the async call
    filename = "test02";
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);
    assert_true(create_dummy_file(UPLOAD_DIR, filename));
    will_return(pcm_restore, amxd_status_unknown_error);
    assert_int_equal(_WriteUsersetting(NULL, NULL, &args, NULL), amxd_status_ok);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Ongoing");
    amxut_bus_handle_events();    // call event handler so async done handler is called
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);
}

void test_cannot_read(UNUSED void** state) {
    amxc_var_t args;
    const char* filename = "test03";
    const char* usersetting_status = NULL;

    //Failure of the async call
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);

    will_return(pcm_backup, amxd_status_unknown_error);
    assert_int_equal(_ReadUsersetting(NULL, NULL, &args, NULL), amxd_status_ok);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Ongoing");
    amxut_bus_handle_events(); // call event handler so async done handler is called
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);

    //Provide empty string as FileName
    filename = "";
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);
    assert_int_equal(_ReadUsersetting(NULL, NULL, &args, NULL), amxd_status_unknown_error);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    amxc_var_clean(&args);

    //Could not find bus for the pcm object
    filename = "test03";
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);
    set_who_has_to_fail(true);
    assert_int_equal(_ReadUsersetting(NULL, NULL, &args, NULL), amxd_status_unknown_error);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Error");
    set_who_has_to_fail(false);
    amxc_var_clean(&args);
}

void test_read_write_no_arguments(UNUSED void** state) {
    assert_int_equal(_ReadUsersetting(NULL, NULL, NULL, NULL), amxd_status_unknown_error);
    assert_int_equal(_WriteUsersetting(NULL, NULL, NULL, NULL), amxd_status_unknown_error);
}

void test_can_read_write(UNUSED void** state) {
    amxc_var_t args;
    const char* filename = "testfile.tar";
    const char* usersetting_status = NULL;
    amxc_string_t command;

    amxc_string_init(&command, 0);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FileName", filename);

    assert_true(create_dummy_file(BACKUP_DIR, filename));
    will_return(pcm_backup, amxd_status_ok);
    will_return(pcm_restore, amxd_status_ok);
    assert_int_equal(_ReadUsersetting(NULL, NULL, &args, NULL), amxd_status_ok);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Ongoing");
    amxut_bus_handle_events(); // call event handler so async done handler is called
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Success");

    assert_true(create_dummy_file(UPLOAD_DIR, filename));
    will_return(pcm_AddBackupFile, amxd_status_ok);
    assert_int_equal(_WriteUsersetting(NULL, NULL, &args, NULL), amxd_status_ok);
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Ongoing");
    amxut_bus_handle_events(); // call event handler so async done handler is called
    usersetting_status = object_const_char(amxd_dm_findf(amxut_bus_dm(), "DeviceInfo."), "UsersettingStatus");
    assert_string_equal(usersetting_status, "Success");

    amxc_string_setf(&command, "rm -f %s/%s", DOWNLOAD_DIR, filename);
    assert_int_equal(0, system(amxc_string_get(&command, 0)));

    amxc_var_clean(&args);
    amxc_string_clean(&command);
}

void test_can_set_vendor_log_file(UNUSED void** state) {
    const char* alias = NULL;
    amxc_var_t args;
    amxc_var_t* data = NULL;
    amxc_var_t* mod_data = NULL;
    amxc_var_t expected;

    amxc_var_init(&args);
    amxc_var_init(&expected);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&expected, AMXC_VAR_ID_HTABLE);

    amxut_bus_handle_events(); // call event handler so async done handler is called
    alias = "vendor_debug";
    //Set a VendorLogFile instance with Alias [vendor_debug]
    amxc_var_add_key(cstring_t, &args, "Alias", alias);
    data = amxc_var_add_key(amxc_htable_t, &args, "Data", NULL);
    amxc_var_add_key(cstring_t, data, "Name", "/tmp/vendor_log_file_test");
    amxc_var_add_key(int32_t, data, "MaximumSize", 0);
    amxc_var_add_key(bool, data, "Persistent", false);
    assert_int_equal(_SetVendorLogFile(NULL, NULL, &args, NULL), 0);
    amxut_bus_handle_events(); // call event handler so async done handler is called

    //Checks if the instance exists and has correct values
    amxc_var_add_key(cstring_t, &expected, "Alias", alias);
    amxc_var_add_key(cstring_t, &expected, "Name", "file:///tmp/vendor_log_file_test");
    amxc_var_add_key(int32_t, &expected, "MaximumSize", 0);
    amxc_var_add_key(bool, &expected, "Persistent", false);
    assert_params_value("DeviceInfo.VendorLogFile.vendor_debug.", &expected);
    assert_nr_instances("DeviceInfo.VendorLogFile.", 1);

    //Modify values of the existing instance with the Alias [vendor_debug]
    mod_data = amxc_var_take_key(data, "Name");
    amxc_var_delete(&mod_data);
    amxc_var_add_key(cstring_t, data, "Name", "/tmp/new_vendor_log_file");
    assert_int_equal(_SetVendorLogFile(NULL, NULL, &args, NULL), 0);
    amxut_bus_handle_events(); // call event handler so async done handler is called

    //Checks if the instance exists and has correct values
    mod_data = amxc_var_take_key(&expected, "Name");
    amxc_var_delete(&mod_data);
    amxc_var_add_key(cstring_t, &expected, "Name", "file:///tmp/new_vendor_log_file");
    assert_params_value("DeviceInfo.VendorLogFile.vendor_debug.", &expected);
    assert_nr_instances("DeviceInfo.VendorLogFile.", 1);

    amxc_var_clean(&args);
    amxc_var_clean(&expected);
}

