/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "test_hostname.h"
#include "deviceinfo_system_hostname.h"

#define DEFAULT_TEST_HOSTNAME "start-hostname"

static char mocked_hostname[HOST_NAME_MAX + 1] = DEFAULT_TEST_HOSTNAME;

#define assert_hosts_equal(name) assert_files_equal(deviceinfo_system_hostname.hosts_file, "hosts/" name)

int __wrap_gethostname(char* name, size_t len) {
    // no expect_check because this function is not really important
    assert_true(len > 0);
    snprintf(name, len, mocked_hostname);
    return mock_type(int);
}

int __wrap_sethostname(const char* name, size_t len) {
    check_expected(name);
    assert_true(len == strlen(name));
    assert_non_null(strncpy(mocked_hostname, name, sizeof(mocked_hostname) - 1));
    return mock_type(int);
}

static void assert_files_equal(const char* a, const char* b) {
    char* linea = NULL;
    size_t lena = 0;
    char* lineb = NULL;
    size_t lenb = 0;
    ssize_t read = 0;

    FILE* fpa = fopen(a, "r");
    FILE* fpb = fopen(b, "r");

    assert_non_null(fpa);
    assert_non_null(fpb);

    while((read = getline(&linea, &lena, fpa)) != -1) {
        assert_int_equal(getline(&lineb, &lenb, fpb), read);
        assert_string_equal(linea, lineb);
    }

    free(linea);
    fclose(fpa);
    free(lineb);
    fclose(fpb);
}

static void expect_get_and_set_hostname(const char* hostname) {
    will_return(__wrap_gethostname, 0);
    expect_string(__wrap_sethostname, name, hostname);
    will_return(__wrap_sethostname, 0);
}

static void system_setup(void) {
    assert_non_null(strncpy(mocked_hostname, DEFAULT_TEST_HOSTNAME, sizeof(mocked_hostname) - 1));

    assert_true(mkdir("/tmp/test-deviceinfo-system", S_IRWXU | S_IRWXG) == 0 || errno == EEXIST);
    deviceinfo_system_hostname.hosts_file = "/tmp/test-deviceinfo-system/hosts";
    deviceinfo_system_hostname.hosts_tmp_file = "/tmp/test-deviceinfo-system/hosts.new";
    system("cp hosts/original /tmp/test-deviceinfo-system/hosts");
}

int test_setup(UNUSED void** state) {
    amxut_bus_setup(NULL);

    sahTraceSetLevel(TRACE_LEVEL_INFO);
    sahTraceAddZone(TRACE_LEVEL_INFO, "system");

    amxut_resolve_function("deviceinfo_system_hostname_changed", _deviceinfo_system_hostname_changed);
    amxut_resolve_function("deviceinfo_system_hostname_init", _deviceinfo_system_hostname_init);

    system_setup();

    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), false);
    amxut_dm_load_odl("../odl/deviceinfo-system.odl");
    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), true);

    amxp_sigmngr_add_signal(NULL, "wait:done");

    expect_get_and_set_hostname("prplOS");
    amxd_object_emit_signal(amxd_dm_get_object(amxut_bus_dm(), "System"), "app:start", NULL);

    assert_int_equal(_deviceinfo_system_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxut_bus_handle_events();

    amxd_object_emit_signal(amxd_dm_get_object(amxut_bus_dm(), "System"), "app:stop", NULL);
    assert_int_equal(_deviceinfo_system_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    amxut_bus_teardown(NULL);
    return 0;
}

void test_sets_hostname_at_startup(UNUSED void** state) {
    // in setup
    assert_hosts_equal("prplOS");
}

void test_hostname_can_be_changed(UNUSED void** state) {
    expect_get_and_set_hostname("Test124");
    amxut_dm_param_set(cstring_t, "System", "HostName", "Test124");
    amxut_dm_param_equals(cstring_t, "System", "HostName", "Test124");
    amxut_bus_handle_events();

    assert_hosts_equal("Test124");
}
